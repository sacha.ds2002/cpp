/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   BitcoinExchange.hpp                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/04/18 10:36:30 by sada-sil          #+#    #+#             */
/*   Updated: 2024/05/24 12:55:09 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef BITCOINEXCHANGE_HPP
# define BITCOINEXCHANGE_HPP
# include "../Bitcoin.h"

class BitcoinExchange
{
    public:
        BitcoinExchange(void);
        BitcoinExchange(const BitcoinExchange& other);
        BitcoinExchange &operator=(const BitcoinExchange &other);
        ~BitcoinExchange();

		void FillMap(std::string fileName);
		void ParseInput(std::string fileName);

	private:
		bool _CheckDate(std::string date) const;
		void _CheckValue(std::string value) const;
		float _GetValue(std::string date, float value);
		
		std::map<std::string, float> _data;
};

#endif

