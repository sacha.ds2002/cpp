/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   BitcoinExchange.cpp                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/04/18 10:36:15 by sada-sil          #+#    #+#             */
/*   Updated: 2024/05/23 16:00:13 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../Bitcoin.h"

// Default constructor
BitcoinExchange::BitcoinExchange(void)
{
    return ;
}

// Copy constructor
BitcoinExchange::BitcoinExchange(const BitcoinExchange &other)
{
    *this = other;
    return ;
}

// Assignment operator overload
BitcoinExchange &BitcoinExchange::operator=(const BitcoinExchange &other)
{
    *this = other;
    return (*this);
}

// Destructor
BitcoinExchange::~BitcoinExchange(void)
{
    return ;
}

void	BitcoinExchange::FillMap(std::string fileName)
{
	std::ifstream data(fileName);
	std::string line;

	if (!data.is_open())
		throw std::runtime_error("Data file not found");
	
	// Check if getline works, and skip first line which isn't values
	if (!std::getline(data, line))
		throw std::runtime_error("Couldn't read data file.");
	
	// Get all dates and values and insert them in the _map
	while (std::getline(data, line))
	{
		std::istringstream stream(line);
		std::string date;
		std::string value;
        if (std::getline(stream, date, ','))
			if (std::getline(stream, value, ','))
				_data.insert(std::pair<std::string, float>(date, std::strtof(value.c_str(), NULL)));
	}
}

void	BitcoinExchange::ParseInput(std::string fileName)
{
	std::ifstream data(fileName);
	std::string line;

	if (!data.is_open())
		throw std::runtime_error("Input file not found");
	
	// Check if getline works, and skip first line which isn't values
	if (!std::getline(data, line))
		throw std::runtime_error("Couldn't read input file.");
	
	// Get all dates and values and insert them in the _map
	while (std::getline(data, line))
	{
		try
		{
			std::istringstream stream(line);
			std::string date;
			std::string value;
			if (std::getline(stream, date, '|'))
			{
				if (!_CheckDate(date))
					throw  std::runtime_error("bad input => " + date);
				if (std::getline(stream, value, '|'))
				{
					_CheckValue(value);
					float v = this->_GetValue(date, std::strtof(value.c_str(), NULL));
					std::cout << date << "=>" << value  << " = " << v << std::endl;
				}
			}	
		}
		catch(const std::exception& e)
		{
			std::cerr << RED << "Error: " <<  e.what() << WHITE << std::endl;
		}	
	}
}

bool	BitcoinExchange::_CheckDate(std::string date) const
{
	int	year;	int	month;	int	day;	char sep;

	if (date.length() == 0)
		throw std::runtime_error("Date is empty.");

	std::istringstream stream(date);
	stream >> year >> sep >> month >> sep >> day;
	if (year < 0 || month < 1 || month > 12 || day < 1 || day > 31)
		return (false);
	if ((month == 4 || month == 6 || month == 9 || month == 11) && day > 30)
		return (false);
	else if (month == 2)
	{
		if (year % 4 == 0) // If the year is divisible by 4, it is leap
		{
			if (day > 29)
				return (false);
		}
		else
			if (day > 28)
				return (false);
	}
	return (true);
}

void BitcoinExchange::_CheckValue(std::string value) const
{
	try
	{
		float v = std::strtof(value.c_str(), NULL);
		if (v < 0)
			throw  std::runtime_error("Negative number.");
		if (v > 1000)
			throw  std::runtime_error("Number too big.");
	}
	catch(const std::invalid_argument &e)
	{
		throw  std::runtime_error("Number couldn't be converted to float.");
	}
}

float BitcoinExchange::_GetValue(std::string date, float value)
{
	std::map<std::string, float>::iterator it;
	int	year;	int	month;	int	day;	char sep;
	std::istringstream stream(date);
	stream >> year >> sep >> month >> sep >> day;

	for (it = _data.begin(); it != _data.end(); it++)
	{
		int	y;	int	m;	int	d;	char s;
		std::istringstream ss(it->first);
		ss >> y >> s >> m >> s >> d;
		if (y == year && m == month && d == day)
			return (value * it->second);
		else if (y >= year && m >= month && d >= day)
			return (value * (--it)->second);
	}
	return 0;
}

