/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/04/18 10:35:40 by sada-sil          #+#    #+#             */
/*   Updated: 2024/05/23 15:58:53 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../Bitcoin.h"

int main(int ac, char **av)
{
	if (ac != 2)
		std::cout << RED << "You must input a database containing the dates and values to test. (input_test.csv)"
			<< RESET << std::endl;
	else
	{
		try
		{
			BitcoinExchange btc;
			btc.FillMap("data.csv");
			btc.ParseInput(av[1]);
		}
		catch(const std::exception& e)
		{
			std::cerr << RED << e.what() << RESET << std::endl;
		}
	
	}
	return 0;
}