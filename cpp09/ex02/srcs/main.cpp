/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/04/18 10:35:40 by sada-sil          #+#    #+#             */
/*   Updated: 2024/05/01 11:52:17 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../PMergeMe.h"

int main(int ac, char **av)
{
	int i = 4;
	if (ac < 2)
		std::cout << RED << "You must input at least 2 number"
			<< RESET << std::endl;
	else
	{
		PMergeMe p;
		p.ParseArgs(ac, av);
		p.SortList();
		p.SortVector();
	}
	return 0;
}