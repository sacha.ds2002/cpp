/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   PMergeMe.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/04/18 10:36:15 by sada-sil          #+#    #+#             */
/*   Updated: 2024/05/24 13:08:22 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../PMergeMe.h"

// Default constructor
PMergeMe::PMergeMe(void)
{
	_list = std::list<int>();
	_vector = std::vector<int>();
    return ;
}

// Copy constructor
PMergeMe::PMergeMe(const PMergeMe &other)
{
    *this = other;
    return ;
}

// Assignment operator overload
PMergeMe &PMergeMe::operator=(const PMergeMe &other)
{
    *this = other;
    return (*this);
}

// Destructor
PMergeMe::~PMergeMe(void)
{
    return ;
}

bool PMergeMe::_IsDuplicate(int nb)
{
    return std::find(_list.begin(), _list.end(), nb) != _list.end();
}

void	PMergeMe::ParseArgs(int ac, char **av)
{
	try
	{
		int i;
		for (i = 1; i < ac; i++)
		{
			std::string str = av[i];
			int nb = std::atoi(str.c_str());
			if (nb < 0)
				throw std::runtime_error("Input integers must be positive.");
			if (i < 5)
				std::cout << nb << " ";
			if (!_IsDuplicate(nb))
			{
				_list.push_back(nb);
				_vector.push_back(nb);
			}
		}
		if (i > 5)
			std::cout << "[...]";
		std::cout << std::endl;
	}
	catch(const std::exception& e)
	{
		std::cerr << RED << e.what() << RESET << '\n';
	}
}

void PMergeMe::SortList(void)
{
    PairList pairs;
	int last = -1;
	
	_PrintList("Before:", 8, YELLOW);
	clock_t start = clock();
	
	_CreateListPairs(pairs);
	if (_list.size() % 2 != 0)
		last = _list.back();
	_list.clear();
	
	_RecursivePairListSort(pairs);
	for (PairList::iterator it = pairs.begin(); it != pairs.end(); it++)
		_list.push_back((*it).second);
	
	try
	{
		for (PairList::iterator it = pairs.begin(); it != pairs.end(); it++)
			_BinarySearchInsertList((*it).first);
		if (last != -1)
			_BinarySearchInsertList(last);
	}
	catch (const std::exception &e)
	{
		std::cout << RED << e.what() << RESET << std::endl;
		return ;
	}

	clock_t end = clock();
	double duration = static_cast<double>(end - start);
	_PrintList("After:", 8, BLUE);
	std::cout << MAGENTA << "Time to process a range of " << _list.size()
		<< " elements with std::list:  \t"<< RESET << duration << " μs, or "
		<< duration / 1000.0 << " ms." << std::endl;
}

void PMergeMe::_CreateListPairs(PairList &pairs)
{
	std::list<int>::iterator it;
	bool first = true;

	for (it = _list.begin(); it != _list.end(); it++)
	{
		if (first == true)
			pairs.push_back(IntPair((*it), 0));
		else
		{
			if ((*it) < pairs.back().first)
			{
				pairs.back().second = pairs.back().first;
				pairs.back().first = (*it);
			}
			else
				pairs.back().second = (*it);
		}
		first = !first;
	}
	if (_list.size() % 2 != 0)
		pairs.remove(pairs.back());
}

void PMergeMe::_BinarySearchInsertList(int nb)
{	
	std::list<int>::const_iterator left = _list.begin();
	std::list<int>::const_iterator right = _list.end();
	right--;

	while (left != right && std::distance(left, right) > 1) 
	{
		std::list<int>::const_iterator mid = left;
		std::advance(mid, std::distance(left, right) / 2);

		if (*mid == nb)
			return ;
		else if (*mid < nb) 
		{
			left = mid;
			left++; // Target is in the right half
		}
		else
		{
			right = mid;
			right--; // Target is in the left half
		}
	}
	
	if (left != _list.end() && *left == nb)
		return ;
	else
	{
		if (*left > nb)
			_list.insert(left, nb);	
		else
		{
			if (*right < nb)
				right++;
			_list.insert(right, nb);
		}
	}
}

void PMergeMe::_RecursivePairListSort(PairList &pairs)
{
    if (pairs.size() <= 1) {
        return;
    }

    // Divide the list into two halves
    PairList firstHalf;
    PairList secondHalf;
    PairList::iterator midpoint = pairs.begin();
    std::advance(midpoint, pairs.size() / 2); // Place the midpoint in the middle of the list
    std::copy(pairs.begin(), midpoint, std::back_inserter(firstHalf));
    std::copy(midpoint, pairs.end(), std::back_inserter(secondHalf));

    // Recursively sort each half
    _RecursivePairListSort(firstHalf);
    _RecursivePairListSort(secondHalf);

    // Merge the sorted halves back together
    pairs.clear(); // Clear the original list
    while (!firstHalf.empty() && !secondHalf.empty()) {
        if (firstHalf.front().second < secondHalf.front().second) {
            pairs.push_back(firstHalf.front());
            firstHalf.pop_front();
        } else {
            pairs.push_back(secondHalf.front());
            secondHalf.pop_front();
        }
    }
    // Append any remaining elements from the first half
    std::copy(firstHalf.begin(), firstHalf.end(), std::back_inserter(pairs));
    // Append any remaining elements from the second half
    std::copy(secondHalf.begin(), secondHalf.end(), std::back_inserter(pairs));
}

void PMergeMe::_PrintList(std::string text, int limit, std::string color)
{
	std::cout << color << text << "\t" << RESET;
	std::list<int>::iterator it;
	for (it = _list.begin(); it != _list.end(); it++)
	{
		if (limit-- <= 0)
		{
			std::cout << "[...]";
			break ;
		}
		std::cout << (*it) << "\t";
	}
	std::cout << std::endl;
}

void PMergeMe::SortVector(void)
{
	PairVector pairs;
	int last = -1;

	clock_t start = clock();

	_CreateVectorPairs(pairs);
	if (_vector.size() % 2 != 0)
		last = _vector.back();
	_vector.clear();

	_RecursivePairVectorSort(pairs);
	for (PairVector::iterator it = pairs.begin(); it != pairs.end(); it++)
		_vector.push_back((*it).second);
	
	try
	{
		for (PairVector::iterator it = pairs.begin(); it != pairs.end(); it++)
			_BinarySearchInsertVector((*it).first);
		if (last != -1)
			_BinarySearchInsertVector(last);
	}
	catch (const std::exception &e)
	{
		
		std::cout << std::endl << RED << e.what() << RESET << std::endl;
		return ;
	}

	clock_t end = clock();
	double duration = static_cast<double>(end - start);
	std::cout << CYAN << "Time to process a range of " << _vector.size()
		<< " elements with std::vector:\t" << RESET << duration << " μs, or "
		<< duration / 1000 << " ms." << std::endl;
}

void PMergeMe::_CreateVectorPairs(PairVector &pairs)
{
	std::vector<int>::iterator it;
	bool first = true;

	for (it = _vector.begin(); it != _vector.end(); it++)
	{
		if (first == true)
			pairs.push_back(IntPair((*it), 0));
		else
		{
			if ((*it) < pairs.back().first)
			{
				pairs.back().second = pairs.back().first;
				pairs.back().first = (*it);
			}
			else
				pairs.back().second = (*it);
		}
		first = !first;
	}
	if (_vector.size() % 2 != 0)
		pairs.pop_back();
}

void PMergeMe::_BinarySearchInsertVector(int nb)
{	
	std::vector<int>::const_iterator left = _vector.begin();
	std::vector<int>::const_iterator right = _vector.end();
	right--;

	while (left != right && std::distance(left, right) > 1) 
	{
		std::vector<int>::const_iterator mid = left;
		std::advance(mid, std::distance(left, right) / 2);

		if (*mid == nb)
			return ;
		else if (*mid < nb)
		{
			left = mid;
			left++; // Target is in the right half
		}
		else
		{
			right = mid;
			right--; // Target is in the left half
		}
	}
	
	if (left != _vector.end() && *left == nb)
		return ;
	else
	{
		if (*left > nb)
			_vector.insert(left, nb);	
		else
		{
			if (*right < nb)
				right++;
			_vector.insert(right, nb);
		}
	}
}

void PMergeMe::_RecursivePairVectorSort(PairVector &pairs)
{
    if (pairs.size() <= 1) {
        return;
    }

    // Divide the list into two halves
    PairVector firstHalf;
    PairVector secondHalf;
    PairVector::iterator midpoint = pairs.begin();
    std::advance(midpoint, pairs.size() / 2); // Place the midpoint in the middle of the list
    std::copy(pairs.begin(), midpoint, std::back_inserter(firstHalf));
    std::copy(midpoint, pairs.end(), std::back_inserter(secondHalf));

    // Recursively sort each half
    _RecursivePairVectorSort(firstHalf);
    _RecursivePairVectorSort(secondHalf);

    // Merge the sorted halves back together
    pairs.clear(); // Clear the original list
    while (!firstHalf.empty() && !secondHalf.empty()) {
        if (firstHalf.front().second < secondHalf.front().second) {
            pairs.push_back(firstHalf.front());
            firstHalf.erase(firstHalf.begin());
        } else {
            pairs.push_back(secondHalf.front());
            secondHalf.erase(secondHalf.begin());
        }
    }
    // Append any remaining elements from the first half
    std::copy(firstHalf.begin(), firstHalf.end(), std::back_inserter(pairs));
    // Append any remaining elements from the second half
    std::copy(secondHalf.begin(), secondHalf.end(), std::back_inserter(pairs));
}
