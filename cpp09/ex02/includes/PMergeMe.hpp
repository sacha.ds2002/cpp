/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   PMergeMe.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/04/18 10:36:30 by sada-sil          #+#    #+#             */
/*   Updated: 2024/05/24 12:47:36 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PMERGEME_HPP
# define PMERGEME_HPP
# include "../PMergeMe.h"

typedef std::pair<int, int> IntPair;
typedef std::list< IntPair > PairList;
typedef std::vector< IntPair > PairVector;

class PMergeMe
{
    public:
        PMergeMe(void);
        PMergeMe(const PMergeMe& other);
        PMergeMe &operator=(const PMergeMe &other);
        ~PMergeMe();

		void ParseArgs(int ac, char **av);
		void SortList();
		void SortVector();

	private:
		std::list<int> _list;
		std::vector<int> _vector;

		bool _IsDuplicate(int nb);

		void _CreateListPairs(PairList &pairs);
		void _RecursivePairListSort(PairList &pairs);
		void _BinarySearchInsertList(int nb);
		void _PrintList(std::string text, int limit, std::string color);

		void _CreateVectorPairs(PairVector &pairs);
		void _RecursivePairVectorSort(PairVector &pairs);
		void _BinarySearchInsertVector(int nb);
};

#endif

