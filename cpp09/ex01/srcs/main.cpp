/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/04/18 10:35:40 by sada-sil          #+#    #+#             */
/*   Updated: 2024/05/24 12:54:08 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../rpn.h"

int main(int ac, char **av)
{
	if (ac != 2)
		std::cout << RED << "You must input the string containing the operations"
			<< RESET << std::endl;
	else
	{
		RPN rpn;
		rpn.ParseInput(av[1]);
	}
	return 0;
}