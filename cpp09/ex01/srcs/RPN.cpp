/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   RPN.cpp                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/04/18 10:36:15 by sada-sil          #+#    #+#             */
/*   Updated: 2024/05/24 12:51:47 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../rpn.h"

// Default constructor
RPN::RPN(void)
{
    return ;
}

// Copy constructor
RPN::RPN(const RPN &other)
{
    *this = other;
    return ;
}

// Assignment operator overload
RPN &RPN::operator=(const RPN &other)
{
    *this = other;
    return (*this);
}

// Destructor
RPN::~RPN(void)
{
    return ;
}

bool	RPN::ParseInput(char *av)
{
	int counter = 0;
	int i = -1;

	while (av[++i])
	{
		if (av[i] != ' ')
		{
			try
			{
				if (std::isdigit(av[i]))
				{
					if (av[i + 1] && std::isdigit(av[i + 1]))
						throw std::runtime_error("Numbers should be between 0 and 9 included.");
					_stack.push(av[i] - '0');
					counter++;
					if (counter > 2)
						throw std::runtime_error("Inputing more than 2 consecutive operands doesn't respect the RPN.");
				}
				else if (av[i])
				{
					_ParseChar(av[i]);
					counter = 0;
				}
			}
			catch(const std::exception& e)
			{
				std::cerr << RED << "Error: " <<  e.what() << WHITE << std::endl;
				return false;
			}	
		}
	}
	if (_stack.size() > 1)
	{
		std::cout << RED << "The input isn't terminated with an operation" << RESET << std::endl;
		return false;
	}
	std::cout << GREEN << _stack.top() << RESET << std::endl;
	return true;
}

void RPN::_ParseChar(char c)
{
	if (_stack.size() >= 2)
	{
		int tmp2 = _stack.top();
		_stack.pop();
		int tmp1 = _stack.top();
		_stack.pop();
		switch (c)
		{
			case '+':
				_stack.push(tmp1 + tmp2);
				break;
			case '-':
				_stack.push(tmp1 - tmp2);
				break;
			case '*':
				_stack.push(tmp1 * tmp2);
				break;
			case '/':
				_stack.push(tmp1 / tmp2);
				break;
			default:
				throw std::runtime_error("Unrecognized operation. Available operations are +, -, * and /");
		}
	}
	else
	{
		std::stringstream ss;
		ss << '\'' << c << '\'';
		throw std::runtime_error("An error occured with the character: " + ss.str());
	}
}
