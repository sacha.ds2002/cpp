/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/03/26 15:05:21 by sada-sil          #+#    #+#             */
/*   Updated: 2024/05/21 14:01:45 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../iter.h"

template <typename T>
void ft_print(const T& value) 
{
    std::cout << value << " ";
}

int main(void)
{
	std::cout << std::endl << RED << "int" << std::endl;
    int ints[] = {1, 2, 3, 4, 5};
    iter(ints, 5, ft_print<int>);
    std::cout << std::endl;


    std::cout << std::endl << GREEN <<  "float" << std::endl;
    double floats[] = {1.1, 2.2, 3.3, 4.4, 5.5};
    iter(floats, 5, ft_print<float>);
    std::cout << std::endl;

	std::cout << std::endl << BLUE << "string" << std::endl;
    std::string strings[] = {"aa", "bb", "cc", "dd", "ee"};
    iter(strings, 5, ft_print<std::string>);
    std::cout << std::endl;
}