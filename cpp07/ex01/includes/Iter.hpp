/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Iter.hpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/03/26 15:43:41 by sada-sil          #+#    #+#             */
/*   Updated: 2024/03/26 15:58:25 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ITER_HPP
# define ITER_HPP
# include "../iter.h"

template <typename T, typename F>
void iter(T* arr, size_t length, F function) 
{
	for (size_t i = 0; i < length; ++i) 
		function(arr[i]);
}

#endif

