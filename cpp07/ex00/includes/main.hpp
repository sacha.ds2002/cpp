/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.hpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/03/26 15:36:06 by sada-sil          #+#    #+#             */
/*   Updated: 2024/03/26 15:37:23 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../functions.h"

#ifndef MAIN_HPP
# define MAIN_HPP

template < typename T >
void swap(T &x, T &y)
{
	T temp = x;
	x = y;
	y = temp;
}

template < typename T >
T const &min(T const &x, T const &y) {
	return (x < y ? x : y);
}

template < typename T >
T const &max(T const &x, T const &y) {
	return (x > y ? x : y);
}

#endif