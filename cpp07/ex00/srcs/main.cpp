/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/03/26 15:05:21 by sada-sil          #+#    #+#             */
/*   Updated: 2024/05/23 11:01:18 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../functions.h"

int main(void)
{
	int a = 2;
	int b = 3;
	swap( a, b );
	std::cout << std::endl << GREEN << "a = " << a << ", b = " << b << std::endl;
	std::cout << "min( a, b ) = " << min( a, b ) << std::endl;
	std::cout << "max( a, b ) = " << max( a, b ) << std::endl;
	
	std::string c = "chaine1";
	std::string d = "chaine2";
	swap(c, d);
	std::cout << std::endl << BLUE  << "c = " << c << ", d = " << d << std::endl;
	std::cout << "min( c, d ) = " << ::min( c, d ) << std::endl;
	std::cout << "max( c, d ) = " << ::max( c, d ) << RESET << std::endl;
	
	return 0;
}