/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Array.tpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/04/03 10:25:12 by sada-sil          #+#    #+#             */
/*   Updated: 2024/05/23 12:55:10 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ARRAY_HPP
# define ARRAY_HPP
# include "../array.h"

template < typename T >
class Array
{
    public:
        Array<T>(void);
		Array<T>(unsigned int n);
        Array<T>(const Array<T> &other);
        Array &operator=(const Array<T> &other);
        ~Array<T>();
		
		unsigned int Size(void) const;

		T& operator[](unsigned int i);

	private:
		T *_array;
		unsigned int _size;
};

// Default constructor
template < typename T>
Array<T>::Array(void)
{
    // std::cout << "Default constructor called" << std::endl;
	this->_array = new T[0];
	this->_size = 0;
    return ;
}

// Parameter constructor
template < typename T>
Array<T>::Array(unsigned int n)
{
    // std::cout << "Parameter constructor called" << std::endl;
	this->_array = new T[n];
	this->_size = n;
    return ;
}

// Copy constructor
template < typename T>
Array<T>::Array(const Array<T> &other)
{
    // std::cout << "Copy constructor called" << std::endl;
    this->_array = new T[other.Size()];
	for (int i = 0; i < other.Size(); i++)
		this->_array[i] = other._array[i];
	this->_size = other.Size();
    return ;
}

// Assignment operator overload
template < typename T>
Array<T> &Array<T>::operator=(const Array<T> &other)
{
	if (this != &other)
	{
		// std::cout << "Assignment operator called" << std::endl;
		delete[] this->_array;
		this->_size = other.Size();
		
		this->_array = new T[this->_size];
		for (int i = 0; i < this->_size; i++)
			this->_array[i] = other._array[i];
		return (*this);	
	}
}

// Destructor
template < typename T>
Array<T>::~Array(void)
{
    // std::cout << "Destructor called" << std::endl;
	delete[] this->_array;
    return ;
}

template < typename T>
unsigned int Array<T>::Size(void) const
{
    return this->_size;
}

template <typename T>
T& Array<T>::operator[](unsigned int i) 
{
	if (i >= this->Size()) {
        throw std::out_of_range("Index is out of bound");
    }
	return this->_array[i];
}

#endif

