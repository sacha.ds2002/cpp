/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/03/26 15:05:21 by sada-sil          #+#    #+#             */
/*   Updated: 2024/05/21 14:11:04 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../array.h"

int main(void)
{	
	Array<int> *a = new Array<int>(10);
	std::cout << std::endl << WHITE << "Size: " << a->Size() << RESET << std::endl;
	
	(*a)[9] = 10;
	std::cout << std::endl << BLUE << "Tenth element = " << (*a)[9] << RESET << std::endl;
	
	try
	{
		std::cout << std::endl << WHITE << "Let's try to get out of bound." << RESET << std::endl;
		(*a)[10] = 11;
	}
	catch (std::exception &e)
	{
		std::cout << RED << e.what() << RESET << std::endl;
	}
	
	std::cout << std::endl << BLUE << "Let's create b using copy constructor" << RESET << std::endl;
	Array<int> *b = new Array<int>(*a);
	
	std::cout << BLUE << "Let's assign a different value to b[9]: 99 instead of 10" << RESET << std::endl << std::endl;
	(*b)[9] = 99;
	std::cout << WHITE << "Result:" << std::endl
		<< "a tenth element: " << (*a)[9] << std::endl
		<< "b tenth element: " << (*b)[9] << RESET << std::endl;
		
	std::cout << std::endl << GREEN << "This test ensured that a deep copy has been done" << RESET << std::endl << std::endl;
	
	delete a;
	delete b;
	return 0;
}
