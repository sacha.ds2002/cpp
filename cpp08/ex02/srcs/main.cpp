/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/03/26 15:05:21 by sada-sil          #+#    #+#             */
/*   Updated: 2024/05/23 15:12:38 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../MutantStack.h"

void	test_mstack(void)
{
	MutantStack<int> mstack;
	std::cout << BLUE << "Add 17 then 5 to the mutant stack." << RESET << std::endl;
	
	mstack.push(17);
	mstack.push(5);
	std::cout << BLUE << "Stack top: "  << RESET << mstack.top() << std::endl;
	mstack.pop();
	std::cout << BLUE << "Stack size after pop: " << RESET << mstack.size() << std::endl;
	
	std::cout << std::endl << BLUE << "Add more numbers to the stack and use iterators to print the whole stack:" << WHITE << std::endl;
	mstack.push(3);
	mstack.push(5);
	mstack.push(737);
	mstack.push(0);	
	MutantStack<int>::iterator it = mstack.begin();
	MutantStack<int>::iterator ite = mstack.end();
	++it;
	--it;
	while (it != ite)
	{
		std::cout << *it << std::endl;
		++it;
	}
	std::stack<int> s(mstack);
}

void	test_list(void)
{
	std::list<int> list;
	std::cout << GREEN << "Add 17 then 5 to the mutant stack." << RESET << std::endl;
	
	list.push_back(17);
	list.push_back(5);
	std::cout << GREEN << "List top: "  << RESET << list.back() << std::endl;
	list.pop_back();
	std::cout << GREEN << "List size after pop: " << RESET << list.size() << std::endl;
	
	std::cout << std::endl << GREEN << "Add more numbers to the list and use iterators to print the whole list:" << WHITE << std::endl;
	list.push_back(3);
	list.push_back(5);
	list.push_back(737);
	list.push_back(0);	
	std::list<int>::iterator it = list.begin();
	std::list<int>::iterator ite = list.end();
	++it;
	--it;
	while (it != ite)
	{
		std::cout << *it << std::endl;
		++it;
	}
	std::list<int> s(list);
}

int main(void)
{
	std::cout << std::endl << MAGENTA << "Tests with a MutantStack:" << WHITE << std::endl;
	test_mstack();
	std::cout << std::endl << MAGENTA << "Tests with a List:" << WHITE << std::endl;
	test_list();
	std::cout << std::endl << MAGENTA << "We can see that the results are the same :)" << WHITE << std::endl;
	return 0;
}