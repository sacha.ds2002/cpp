/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   MutantStack.tpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/04/04 14:59:37 by sada-sil          #+#    #+#             */
/*   Updated: 2024/04/17 14:30:19 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MUTANTSTACK_TPP
# define MUTANTSTACK_TPP
# include "../MutantStack.h"

template <typename T>
class MutantStack : public std::stack<T>
{
    public:
        MutantStack<T>(void);
        MutantStack<T>(const MutantStack<T> &other);
		MutantStack<T> &operator=(const MutantStack<T> &other);
        ~MutantStack<T>(void);

		typedef typename std::stack<T>::container_type::iterator iterator;

		iterator begin(void);
		iterator end(void);
};

template <typename T>
MutantStack<T>::MutantStack(void)
{ }

template <typename T>
MutantStack<T>::MutantStack(const MutantStack<T> &other)
{ 
	*this = other; 
}

template <typename T>
MutantStack<T> &MutantStack<T>::operator=(const MutantStack<T> &other)
{
	*this = other;
	return (*this);
}

template <typename T>
MutantStack<T>::~MutantStack(void)
{ }

template <typename T>
typename std::stack<T>::container_type::iterator MutantStack<T>::begin(void)
{
	return (this->c.begin());
}

template <typename T>
typename std::stack<T>::container_type::iterator MutantStack<T>::end(void)
{
	return (this->c.end());
}

#endif
