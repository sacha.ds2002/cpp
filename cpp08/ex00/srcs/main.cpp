/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/03/26 15:05:21 by sada-sil          #+#    #+#             */
/*   Updated: 2024/05/23 14:23:02 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../easyfind.h"

int main(int ac, char **av)
{
	std::vector<int> container;
	
	if (ac != 2)
	{
		std::cerr << RED << "Need one argument: number searched in the list." << std::endl;
		return 1;
	}
	
	try
	{
		// Fill the container with integers from 0 to 100, but only even numbers
		for (int i = 0; i <= 100; i += 2) 
			container.push_back(i);

		// Search the number input as parameter in the list, throw an error if not found
		int ret = Easyfind(container, std::atoi(av[1]));
		std::cout << GREEN << "Integer " << ret << " has been found in the container."
				<< RESET << std::endl;
	}
	catch(const std::exception& e)
	{
		std::cerr << RED << e.what() << std::endl;
	}
}