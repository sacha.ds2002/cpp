/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Easyfind.tpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/03/26 15:43:41 by sada-sil          #+#    #+#             */
/*   Updated: 2024/04/04 14:16:15 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef EASYFIND_TPP
# define EASYFIND_TPP
# include "../easyfind.h"

template <typename T>
const int &Easyfind(const T &container, int find) 
{
	typename T::const_iterator it;
    for (it = container.begin(); it != container.end(); ++it) {
        if (*it == find)
			return *it;
    }
	throw std::runtime_error("Value not found in container.");
}

#endif

