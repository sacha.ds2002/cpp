/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   span.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/04/04 14:58:29 by sada-sil          #+#    #+#             */
/*   Updated: 2024/04/04 14:58:29 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SPAN_H
# define SPAN_H

# define RESET			"\033[0m"
# define RED			"\033[0;31m"
# define GREEN			"\033[0;32m"
# define YELLOW			"\033[0;33m"
# define BLUE			"\033[0;34m"
# define MAGENTA		"\033[0;35m"
# define CYAN			"\033[0;36m"
# define WHITE			"\033[0;37m"

# include <iostream>
# include <string>
# include <iomanip>
# include <vector>
# include <list>
# include <array>
# include <algorithm>
#include <cstdlib>
#include <ctime>
# include <random>

# include "includes/Span.hpp"

#endif