/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/03/26 15:05:21 by sada-sil          #+#    #+#             */
/*   Updated: 2024/05/23 14:57:33 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../span.h"

void simple_tests(void)
{
	std::cout << WHITE << "Adding the numbers: 3, 5, 33, 9, 11. " << RESET << std::endl;
	std::cout << std::endl << BLUE << "=== SIMPLE TESTS ===" << RESET << std::endl;
	Span sp = Span(5);
	sp.AddNumber(3);
	sp.AddNumber(5);
	sp.AddNumber(33);
	sp.AddNumber(9);
	sp.AddNumber(11);
	std::cout << YELLOW << "Shortest span: " << sp.ShortestSpan() << RESET << std::endl;
	std::cout << MAGENTA << "Longest span: " << sp.LongestSpan() << RESET << std::endl;

	try
	{
		std::cout << WHITE << "Adding a number when the Span is"
			<< " already full will throw an exception" << RESET << std::endl;
		sp.AddNumber(7);
	}
	catch (const std::exception &e)
	{
		std::cout << RED << e.what() << RESET << std::endl;
	}
}

void empty_span_test(void)
{
	std::cout << std::endl << BLUE << "=== EMPTY SPAN TESTS ===" << RESET << std::endl;
	Span sp = Span(0);
	try
	{
		std::cout << WHITE << "Trying to get the shortest span in an empty Span "
			<< "will throw an exception. Same goes for longest span." << RESET << std::endl;
		std::cout << YELLOW << "Shortest span: " << sp.ShortestSpan() << RESET << std::endl;
	}
	catch(const std::exception& e)
	{
		std::cerr << RED << e.what() << RESET << std::endl;
	}
}

void add_range_test(void)
{
	std::cout << std::endl << BLUE << "=== ADD RANGE TESTS ===" << RESET << std::endl;
	Span sp = Span(200);
	try
	{
		std::cout << WHITE << "Trying to add 200 numbers to the Span." << RESET << std::endl;
		// Create and fill a vector container with random numbers
		std::vector<int> range;
		for (int i = 0; i < 200; i++)
			range.push_back(std::rand() % 999999 + 1);
		// Add all the vector's numbers to the Span using AddRange, then try to sort it
		sp.AddRange(range);
		std::cout << WHITE << "Getting the shortest span." << RESET << std::endl;
		std::cout << YELLOW << "Shortest span: " << sp.ShortestSpan() << RESET << std::endl;
		std::cout << YELLOW << "Longest span: " << sp.LongestSpan() << RESET << std::endl;
	}
	catch(const std::exception& e)
	{
		std::cerr << RED << e.what() << RESET << std::endl;
	}
}

void big_span_test(void)
{
	std::cout << std::endl << BLUE << "=== 100'000 NUMBERS SPAN TESTS ===" << RESET << std::endl;
	Span sp = Span(100000);
	try
	{
		std::cout << WHITE << "Trying to add 10'000 numbers to the Span." << RESET << std::endl;
		// Create and fill a vector container with random numbers
		std::vector<int> range;
		for (int i = 0; i < 100000; i++)
			range.push_back(std::rand() % 99999999 + 1);
		// Add all the vector's numbers to the Span using AddRange, then try to sort it
		sp.AddRange(range);
		std::cout << WHITE << "Getting the shortest span." << RESET << std::endl;
		std::cout << YELLOW << "Shortest span: " << sp.ShortestSpan() << RESET << std::endl;
		std::cout << YELLOW << "Longest span: " << sp.LongestSpan() << RESET << std::endl;
	}
	catch(const std::exception& e)
	{
		std::cerr << RED << e.what() << RESET << std::endl;
	}

}

int main(int ac, char **av)
{
	std::srand(std::time(nullptr));
	simple_tests();
	empty_span_test();
	add_range_test();
	big_span_test();
	return 0;
}