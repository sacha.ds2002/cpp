/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Span.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/04/04 14:59:34 by sada-sil          #+#    #+#             */
/*   Updated: 2024/05/23 14:58:46 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../span.h"

// Default constructor
Span::Span()
{
    return ;
}

// N constructor 
Span::Span(unsigned int n) : _n(n)
{
    return ;
}

// Copy constructor
Span::Span(const Span &other) : _n(other._n)
{
    (void) other;
    return ;
}

// Assignment operator overload
Span &Span::operator=(const Span &other)
{
    this->_n = other._n;
    return (*this);
}

// Destructor
Span::~Span(void)
{
    return ;
}

void	Span::AddNumber(int value)
{
	if (_vec.size() >= this->_n)
		throw std::out_of_range("Tried to insert a number, but exceeded the Span's range.");
	else
		_vec.push_back(value);
}

int Span::ShortestSpan()
{
	if (_vec.size() <= 1)
		throw std::runtime_error("The Span doesn't contain enough numbers.");
	if (!std::is_sorted(_vec.begin(), _vec.end()))
		std::sort(_vec.begin(), _vec.end());	
	std::vector<int>::iterator it = _vec.begin();
	it++;
	return ((*it) - _vec.front());
}

int Span::LongestSpan()
{
	if (_vec.size() <= 1)
		throw std::runtime_error("The Span doesn't contain enough numbers.");
	if (!std::is_sorted(_vec.begin(), _vec.end()))
		std::sort(_vec.begin(), _vec.end());
	return  (_vec.back() - _vec.front());
}

void	Span::AddRange(const std::vector<int> &container)
{
	if ((container.size() + _vec.size()) > _n)
		throw std::out_of_range("The input container is too long to be inserted into the Span");
	_vec.insert(_vec.end(), container.begin(), container.end());
	std::cout << GREEN << "Added " << container.size()
	<< " numbers to the Span." << RESET << std::endl;
}
