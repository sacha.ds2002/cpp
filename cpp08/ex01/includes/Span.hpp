/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Span.hpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/04/04 14:59:37 by sada-sil          #+#    #+#             */
/*   Updated: 2024/05/23 14:32:30 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SPAN_HPP
# define SPAN_HPP
# include "../span.h"

class Span
{
    public:
		Span();
        Span(unsigned int n);
        Span(const Span& other);
        Span &operator=(const Span &other);
        ~Span();
		
		void AddNumber(int value);
		void AddRange(const std::vector<int> &container);
		int ShortestSpan();
		int LongestSpan();

	private:
		unsigned int _n;
		std::vector<int> _vec;
};

#endif
