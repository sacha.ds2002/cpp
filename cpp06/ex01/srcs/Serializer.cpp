/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Serializer.cpp                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/03/14 12:44:03 by sada-sil          #+#    #+#             */
/*   Updated: 2024/05/21 13:29:25 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../serializer.h"

Serializer::Serializer()
{}

Serializer::Serializer(const Serializer& other)
{
	(void) other;
}

Serializer &Serializer::operator=(const Serializer &other)
{
	(void) other;
	return *this;
}

Serializer::~Serializer()
{}

uintptr_t Serializer::Serialize(Data *ptr)
{
	try 
	{
		uintptr_t ret = reinterpret_cast<uintptr_t>(ptr);
		if (!ret)
			throw Serializer::SerializeError();
		return ret;
	}
	catch (std::exception &e)
	{
		std::cout << RED << e.what() << RESET << std::endl;
	}
	return 0;
}

Data *Serializer::Deserialize(uintptr_t raw)
{
	try 
	{
		Data *ret = reinterpret_cast<Data *>(raw);
		if (!ret)
			throw Serializer::DeserializeError();
		return ret;
	}
	catch (std::exception &e)
	{
		std::cout << RED << e.what() << RESET << std::endl;
	}
	return 0;
}

