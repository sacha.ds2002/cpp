/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   DAta.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/03/14 12:45:16 by sada-sil          #+#    #+#             */
/*   Updated: 2024/03/14 13:32:36 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../serializer.h"

// Default constructor
Data::Data(void)
{
    std::cout << "Default constructor called" << std::endl;
	_id = 0;
	_value = "Default";
    return ;
}

// Param constructor
Data::Data(int i, std::string v) : _id(i), _value(v)
{
    std::cout << "Default constructor called" << std::endl;
    return ;
}

// Copy constructor
Data::Data(const Data &other) : _id(other._id), _value(other._value)
{
    std::cout << "Copy constructor called" << std::endl;
    return ;
}

// Assignment operator overload
Data &Data::operator=(const Data &other)
{
    std::cout << "Assignment operator called" << std::endl;
	_id = other._id;
	_value = other._value;
    return (*this);
}

// Destructor
Data::~Data(void)
{
    std::cout << "Destructor called" << std::endl;
    return ;
}

int	Data::GetId(void) const
{
	return _id;
}

std::string	Data::GetValue(void) const
{
	return _value;
}
