/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/03/05 13:17:42 by sada-sil          #+#    #+#             */
/*   Updated: 2024/05/21 13:30:43 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../serializer.h"

int main(void)
{
	Data *d1 = new Data(1, "Test");
	Data *d2;
	uintptr_t ptr;

	std::cout << std::endl << MAGENTA << "d1's pointer: " << d1 << RESET << std::endl;
	std::cout << GREEN << "d1's id and value: " << d1->GetId()
		<< " | "  << d1->GetValue() << RESET << std::endl << std::endl;

	std::cout << WHITE << "Serialization of d1" << RESET << std::endl;
	ptr = Serializer::Serialize(d1);
	std::cout << YELLOW << "Uintptr's value: " << ptr << RESET << std::endl;
	std::cout << WHITE << "Deserialization into d2" << RESET << std::endl << std::endl;
	d2 = Serializer::Deserialize(ptr);

	std::cout << MAGENTA << "d2's pointer: " << d2 << RESET << std::endl;
	std::cout << GREEN << "d2's id and value: " << d2->GetId()
		<< " | "  << d2->GetValue() << RESET << std::endl << std::endl;

	delete d2;
	return 0;
}