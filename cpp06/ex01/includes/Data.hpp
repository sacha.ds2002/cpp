/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Data.hpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/03/14 12:44:50 by sada-sil          #+#    #+#             */
/*   Updated: 2024/03/14 13:13:22 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef DATA_HPP
# define DATA_HPP
# include "../serializer.h"

class Data
{
    public:
        Data(void);
		Data(int i, std::string v);
        Data(const Data& other);
        Data &operator=(const Data &other);
        ~Data();

		int			GetId(void) const;
		std::string GetValue(void) const;

	private:
		int 		_id;
		std::string _value;
};

#endif

