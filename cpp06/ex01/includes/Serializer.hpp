/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Serializer.hpp                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/03/14 12:44:48 by sada-sil          #+#    #+#             */
/*   Updated: 2024/05/21 13:12:44 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SERIALIZER_HPP
# define SERIALIZER_HPP
# include "../serializer.h"

class Serializer
{
    public:
		class SerializeError : public std::exception
		{
			virtual const char *what() const throw()
			{
				return ("The serialization failed.");
			}
		};

		class DeserializeError : public std::exception
		{
			virtual const char *what() const throw()
			{
				return ("The deserialization failed.");
			}
		};
		
		static uintptr_t Serialize(Data* ptr);
		static Data* Deserialize(uintptr_t raw);
	
	private:
		Serializer();
		Serializer(const Serializer& other);
        Serializer &operator=(const Serializer &other);
        ~Serializer();
};

#endif
