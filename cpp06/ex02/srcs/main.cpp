/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/03/14 14:11:21 by sada-sil          #+#    #+#             */
/*   Updated: 2024/03/14 16:30:54 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../base.h"

namespace Utils
{
	Base *generate(void)
	{
		std::random_device rng;
		std::uniform_int_distribution<int> dist(1, 3);
		
		switch (dist(rng))
		{
			case 1:
				std::cout << "Generate A" << std::endl;
				return new A();
				break;
			case 2:
				std::cout << "Generate B" << std::endl;
				return new B();
				break;
			case 3:
				std::cout << "Generate C" << std::endl;
				return new C();
				break;
			default:
				std::cout << RED << "Randomizer failed" << RESET << std::endl;
				break;
		}
		return NULL;
	}

	void identify(Base* p)
	{
		if (dynamic_cast<A*>(p))
		{
			std::cout << GREEN << "* Object type is A" << RESET << std::endl;
		} 
		else if (dynamic_cast<B*>(p)) 
		{
			std::cout << BLUE << "* Object type is type B" << RESET  << std::endl;
		} 
		else if (dynamic_cast<C*>(p)) 
		{
			std::cout << MAGENTA << "* Object type is type C" << RESET  << std::endl;
		} 
		else 
		{
			std::cout << RED << "* Unknown object type" << RESET << std::endl;
		}
	}

	void identify(Base &p)
	{
		try
		{
			try
			{
				A &a = dynamic_cast<A &>(p);
				std::cout << GREEN << "& Object type is A" << RESET << std::endl; 
			}
			catch (std::bad_cast &e)
			{
				try
				{
					B &b = dynamic_cast<B &>(p);
					std::cout << BLUE << "& Object type is B" << RESET << std::endl; 	
				}
				catch (std::bad_cast &e)
				{
					try
					{
						C &c = dynamic_cast<C &>(p);
						std::cout << MAGENTA << "& Object type is C" << RESET << std::endl; 	
					}
					catch (std::bad_cast &e)
					{
						std::cout << RED << "* Unknown object type" << RESET << std::endl;
					}
				}
			}	
		}
		catch (std::exception &e)
		{
			std::cout << RED << e.what() << std::endl;
		}
	}
};

int	main(void)
{
	Base *b = Utils::generate();
	Utils::identify(b);
	Utils::identify(*b);
	delete b;
	return 0;
}