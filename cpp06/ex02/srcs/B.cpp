/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   B.cpp                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/03/14 14:13:12 by sada-sil          #+#    #+#             */
/*   Updated: 2024/03/14 16:31:20 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../base.h"

// Default constructor
B::B(void)
{
    std::cout << BLUE << "B constructor called" << RESET << std::endl;
    return ;
}

// Destructor
B::~B(void)
{
    return ;
}

