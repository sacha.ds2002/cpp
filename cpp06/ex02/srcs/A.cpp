/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   A.cpp                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/03/14 14:13:18 by sada-sil          #+#    #+#             */
/*   Updated: 2024/03/14 16:31:32 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../base.h"

// Default constructor
A::A(void)
{
    std::cout << GREEN << "A constructor called" << RESET << std::endl;
    return ;
}

// Destructor
A::~A(void)
{
    return ;
}

