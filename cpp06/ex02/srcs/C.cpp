/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   C.cpp                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/03/14 14:13:09 by sada-sil          #+#    #+#             */
/*   Updated: 2024/03/14 16:31:06 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../base.h"

// Default constructor
C::C(void)
{
    std::cout << MAGENTA << "C constructor called" << RESET << std::endl;
    return ;
}

// Destructor
C::~C(void)
{
    return ;
}

