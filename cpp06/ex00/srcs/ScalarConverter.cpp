/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ScalarConverter.cpp                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/03/05 13:19:39 by sada-sil          #+#    #+#             */
/*   Updated: 2024/05/21 13:09:18 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../scalar.h"

namespace Utils
{
	bool	IsSpecial(std::string value) 
	{
		int	size = 6;
		std::string specials[6] = {
			"-inff", "+inff", "nanf",
			"-inf", "+inf", "nan",
		};
		
		for (int i = 0; i < size; i++)
		{
			if (value == specials[i])
				return true;
		}
		return false;
	}
	
	bool	IsChar(std::string value) 
	{
		if (value.length() != 1 || std::isdigit(value[0])
			|| !std::isprint(value[0]))
			return false;
		return true;
	}

	bool	IsInt(std::string value)
	{
		for (int i = 0; i < value.length(); i++)
		{
			if (value[i] != '-' && !std::isdigit(value[i]))
				return false;
		}
		return true;
	}

	bool	IsFloat(std::string value)
	{
		int len = value.length();
		
		if (value[len - 1] != 'f')
			return false;
		for (int i = 0; i < len; i++)
		{
			if (value[i] != '-' && !std::isdigit(value[i])
				&& value[i] != 'f' && value[i] != '.')
				return false;
		}
		return true;
	}

	bool	IsDouble(std::string value)
	{
		int len = value.length();
		
		for (int i = 0; i < len; i++)
		{
			if (value[i] != '-' && !std::isdigit(value[i])
				&& value[i] != '.')
				return false;
		}
		return true;
	}

	int	GetType(std::string value)
	{
		if (IsSpecial(value))
			return 0;
		if (IsChar(value))
			return 1;
		if (IsInt(value))
			return 2;
		if (IsFloat(value))
			return 3;
		if (IsDouble(value))
			return 4;
		return -1;
	}

	// print char
	void print(std::string value)
	{
		std::cout << WHITE << "char: impossible" << std::endl;
		std::cout << "int: impossible" << std::endl;
		if (value.find("nan") != std::string::npos)
		{
			std::cout << "float: nanf" << std::endl;
			std::cout << "double: nan" << std::endl;
		}
		else if (value.find("-inf") != std::string::npos)
		{
			std::cout << "float: -inff" << std::endl;
			std::cout << "double: -inf" << std::endl;
		}
		else
		{
			std::cout << "float: inff" << std::endl;
			std::cout << "double: inf" << std::endl;
		}
		std::cout << RESET;
	}

	// print char
	void print(char c)
	{
		int		i = static_cast<int>(c);
		float	f = static_cast<float>(c);
		double	d = static_cast<double>(c);

		if (c >= 32 && c <= 126)
			std::cout << WHITE << "char: \'" << c << "\'"<< std::endl;
		else
			std::cout << WHITE << "char: Non Displayable" << std::endl;
		std::cout << "int: " << i << std::endl;
		std::cout << "float: " << f << ".0f" << std::endl;
		std::cout << WHITE "double: " << d << ".0" << RESET << std::endl;
	}

	// print int
	void print(int i)
	{
		char	c = static_cast<char>(i);
		float	f = static_cast<float>(i);
		double	d = static_cast<double>(i);

		if (c < 0 || c > 127)
			std::cout << WHITE << "char: impossible" << std::endl;
		else if (std::isprint(c))
			std::cout << WHITE << "char: \'" << c << "\'" << std::endl;
		else
			std::cout << WHITE << "char: Non Displayable" << std::endl;
		std::cout << "int: " << i << std::endl;
		std::cout << "float: " << f << ".0f" << std::endl;
		std::cout << "double: " << d << ".0" << RESET << std::endl;
	}

	// print float
	void print(float f)
	{
		char	c = static_cast<char>(f);
		int		i = static_cast<int>(f);
		double	d = static_cast<double>(f);

		if (c < 0 || c > 127)
			std::cout << WHITE << "char: impossible" << std::endl;
		else if (std::isprint(c))
			std::cout << WHITE << "char: \'" << c << "\'" << std::endl;
		else
			std::cout << WHITE << "char: Non Displayable" << std::endl;
			
		std::cout << "int: " << i << std::endl;
		if (f == i)
			std::cout << "float: " << f << ".0f" << std::endl;
		else
			std::cout << "float: " << f << "f" << std::endl;
		if (d == i)
			std::cout << "double: " << d  << ".0" << RESET << std::endl;
		else
			std::cout << "double: " << d << RESET << std::endl;
	}

	// print double
	void print(double d)
	{
		char	c = static_cast<char>(d);
		int		i = static_cast<int>(d);
		float	f = static_cast<float>(d);

		if (c < 0 || c > 127)
			std::cout << WHITE << "char: impossible" << std::endl;
		else if (std::isprint(c))
			std::cout << WHITE << "char: \'" << c << "\'" << std::endl;
		else
			std::cout << WHITE << "char: Non Displayable" << std::endl;
			
		std::cout << "int: " << i << std::endl;
		if (f == i)
			std::cout << "float: " << f << ".0f" << std::endl;
		else
			std::cout << "float: " << f << "f" << std::endl;
		if (d == i)
			std::cout << "double: " << d << ".0" << RESET << std::endl;
		else
			std::cout << "double: " << d << RESET << std::endl;
	}
}

ScalarConverter::ScalarConverter()
{}

ScalarConverter::ScalarConverter(const ScalarConverter& other)
{
	(void) other;
}

ScalarConverter &ScalarConverter::operator=(const ScalarConverter &other)
{
	(void) other;
	return *this;
}

ScalarConverter::~ScalarConverter()
{}

void ScalarConverter::Convert(std::string value)
{
	int	t = 0; // 0 = undefined, 1 = char, 2 = int, 3 = float, 4 = double
	
	t = Utils::GetType(value);
	try
	{
		switch (t)
		{
			case 0:
				Utils::print(value);
				break;
			case 1:
				Utils::print(value[0]);
				break;
			case 2:
				Utils::print(std::stoi(value));
				break;
			case 3:
				Utils::print(std::stof(value));
				break;
			case 4:
				Utils::print(std::stod(value));
				break;
			default:
				throw ScalarConverter::ConversionNotFound();
				break;
		}
	}
	catch(const std::exception& e)
	{
		std::cerr << RED << e.what() << RESET << std::endl;
	}
}