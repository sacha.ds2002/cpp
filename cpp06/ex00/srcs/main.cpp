/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/03/05 13:17:42 by sada-sil          #+#    #+#             */
/*   Updated: 2024/05/21 13:09:27 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../scalar.h"

int main(int ac, char **av)
{
	if (ac != 2)
		std::cout << RED << "Please input one argument." << RESET << std::endl;
	else
		ScalarConverter::Convert(av[1]);
	return 0;
}