/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ScalarConverter.hpp                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/03/05 13:17:45 by sada-sil          #+#    #+#             */
/*   Updated: 2024/05/21 13:06:56 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SCALARCONVERTER_HPP
# define SCALARCONVERTER_HPP
# include "../scalar.h"

class ScalarConverter
{
    public:
		class ConversionNotFound : public std::exception
		{
			virtual const char *what() const throw()
			{
				return ("The input string couldn't be converted.");
			}
		};
		
		static void	Convert(std::string value);
	
	private:
		ScalarConverter();
		ScalarConverter(const ScalarConverter& other);
        ScalarConverter &operator=(const ScalarConverter &other);
        ~ScalarConverter();
};

#endif

