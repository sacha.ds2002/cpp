/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   phonebook.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/17 15:24:54 by sada-sil          #+#    #+#             */
/*   Updated: 2023/11/22 14:28:40 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../phonebook.h"

PhoneBook::PhoneBook(void)
{
	std::cout << BLUE << "PhoneBook Constructor called" << RESET << std::endl;
	_id = 0;
	return;
}

PhoneBook::~PhoneBook(void)
{
	std::cout << BLUE << "PhoneBook Destructor called" << RESET << std::endl;
	return;
}

Contact PhoneBook::GetContact(int index) const
{
	return (_contacts[index - 1]);
}

void	PhoneBook::CreateContact()
{
	std::cout << std::endl;
	_contacts[_id].SetFirstName(GetNonEmptyInput("Enter the first name: "));
	_contacts[_id].SetLastName(GetNonEmptyInput("Enter the last name: "));
	_contacts[_id].SetNickName(GetNonEmptyInput("Enter the nickname: "));
	_contacts[_id].SetPhoneNumber(GetNonEmptyInput("Enter the phone number: "));
	_contacts[_id].SetDarkSecret(GetNonEmptyInput("Enter the darkest secret: "));
	_contacts[_id].SetIndex(_id + 1);

	std::cout << YELLOW << "User created!" << std::endl << std::endl << RESET;

	// Update id for next created user
	if (++_id > 7)
		_id = 0;
}

void	PhoneBook::SearchContact(void) const
{
	int	id;
	if (_contacts[0].GetIndex() == UNSET)
	{
		std::cout << RED << "No contact has been added yet." << RESET << std::endl << std::endl;
		return;
	}
	this->_ShowList();
	while (1)
	{
		try
		{
			id = std::stoi(GetNonEmptyInput("Please input a contact's index: ")) - 1;
			if (id >= 0 && id <= 7)
			{
				if (_contacts[id].GetIndex() != UNSET)
				{
					std::cout << MAGENTA 
						<< "Index: " << RESET << _contacts[id].GetIndex() << std::endl << MAGENTA 
						<< "First name: " << RESET << _contacts[id].GetFirstName() << std::endl << MAGENTA 
						<< "Last name: " << RESET << _contacts[id].GetLastName() << std::endl << MAGENTA 
						<< "Nick name: " << RESET << _contacts[id].GetNickName() << std::endl << MAGENTA 
						<< "Phone number: " << RESET << _contacts[id].GetPhoneNumber() << std::endl << MAGENTA 
						<< "Darkest secret: " << RESET << _contacts[id].GetDarkSecret() << RESET << std::endl << std::endl;
					return;
				}
			}
			std::cout << RED << "This user does not exist." << RESET << std::endl << std::endl;
			return;
		}
		catch(std::exception e)
		{
			std::cout << RED << "Please input a number." << std::endl << std::endl;
			if (std::cin.eof())
				return;
		}
	}
}

void	PhoneBook::_ShowList(void) const
{
	//Print array header
	std::cout << MAGENTA << "|-------------------------------------------|" << std::endl
		<< "|" << RESET << "     Index" << MAGENTA << "|" << RESET << "First name" << MAGENTA
		<< "|" << RESET << " Last name" << MAGENTA << "|" << RESET << " Nick name" << MAGENTA
		<< "|" << MAGENTA << std::endl << "|-------------------------------------------|" << RESET << std::endl;
	
	//Print array content
	for (int i = 0; i < 8; i++)
	{
		if (_contacts[i].GetIndex() == UNSET)
			break;
		std::cout << MAGENTA << "|" << RESET << std::setw(10) << std::to_string(_contacts[i].GetIndex());
		std::cout << MAGENTA << "|" << RESET << std::setw(10) << (_contacts[i].GetFirstName().length() > 10 ?
                 _contacts[i].GetFirstName().substr(0, 9) + "." : _contacts[i].GetFirstName());
		std::cout << MAGENTA << "|" << RESET << std::setw(10) << (_contacts[i].GetLastName().length() > 10 ?
                 _contacts[i].GetLastName().substr(0, 9) + "." : _contacts[i].GetLastName());
		std::cout << MAGENTA << "|" << RESET << std::setw(10) << (_contacts[i].GetNickName().length() > 10 ?
                 _contacts[i].GetNickName().substr(0, 9) + "." : _contacts[i].GetNickName());
		std::cout << MAGENTA << "|" << std::endl << "|-------------------------------------------|" << RESET << std::endl;
	}
	std::cout << std::endl;
}