/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/17 15:45:43 by sada-sil          #+#    #+#             */
/*   Updated: 2023/11/22 14:14:09 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../phonebook.h"

int	main(void)
{
	PhoneBook 	repertory;
	std::string cmd;
	
	while (1)
	{
		std::cout << YELLOW << "Waiting for command: " << RESET;
		std::getline(std::cin, cmd);
		if (std::cin.eof())
			return 1;
		for (int i = 0; cmd[i] != '\0'; i++)
			cmd[i] = std::toupper(cmd[i]);
		if (cmd == "ADD")
			repertory.CreateContact();
		else if (cmd == "SEARCH")
			repertory.SearchContact();
		else if (cmd == "EXIT")
			break;
		else
			std::cout << RED << "Please enter ADD, SEARCH or EXIT." << RESET << std::endl;
	}
	return 0;
}