/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/22 13:20:14 by sada-sil          #+#    #+#             */
/*   Updated: 2023/11/22 14:12:09 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../phonebook.h"

std::string	GetNonEmptyInput(const std::string prompt)
{
	std::string input;
	
	while (input.empty()) {
		std::cout << GREEN << prompt << WHITE;
		std::getline(std::cin, input);
		if (input.empty())
			std::cout << RED << "Empty input" << std::endl;
		if (std::cin.eof())
			input = "eof";
	}
	return input;
}