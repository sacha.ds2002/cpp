/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   contact.cpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/17 16:26:44 by sada-sil          #+#    #+#             */
/*   Updated: 2023/11/22 14:07:00 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../phonebook.h"

Contact::Contact(void)
{
	std::cout << CYAN <<  "Contact Constructor called" << RESET << std::endl;
	_index = UNSET;
	return;
}

Contact::~Contact(void)
{
	std::cout << CYAN << "Contact Destructor called" << RESET << std::endl;
	return;
}

int	Contact::GetIndex(void) const
{
	return (_index);
}

void	Contact::SetIndex(int v)
{
	if (v >= 1 || v <= 8)
		_index = v;
	return;
}

std::string	Contact::GetFirstName(void) const
{
	return (_first);
}

void	Contact::SetFirstName(std::string v)
{
	_first = v;
	return;
}

std::string	Contact::GetLastName(void) const
{
	return (_last);
}

void	Contact::SetLastName(std::string v)
{
	_last = v;
	return;
}

std::string	Contact::GetNickName(void) const
{
	return (_nick);
}

void	Contact::SetNickName(std::string v)
{
	_nick = v;
	return;
}

std::string	Contact::GetPhoneNumber(void) const
{
	return (_phone);
}

void	Contact::SetPhoneNumber(std::string v)
{
	_phone = v;
	return;
}

std::string	Contact::GetDarkSecret(void) const
{
	return (_secret);
}

void	Contact::SetDarkSecret(std::string v)
{
	_secret = v;
	return;
}