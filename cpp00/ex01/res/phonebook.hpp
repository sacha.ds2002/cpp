/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   phonebook.hpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/17 15:24:51 by sada-sil          #+#    #+#             */
/*   Updated: 2023/11/23 11:01:06 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PHONEBOOK_HPP
# define PHONEBOOK_HPP
# include "../phonebook.h"

class PhoneBook
{
	public:
		PhoneBook(void);
		~PhoneBook(void);
	
		Contact GetContact(int index) const;
		void	CreateContact();
		void	SearchContact(void) const;
		
	private:
		void	_ShowList(void) const;
		Contact _contacts[8];
		int		_id;
};

#endif