/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   contact.hpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/17 16:27:00 by sada-sil          #+#    #+#             */
/*   Updated: 2023/11/23 10:57:54 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CONTACT_HPP
# define CONTACT_HPP
# include "../phonebook.h"

// Contact class contains data about a person
class Contact
{
	public:
		// Constructor / Destructor
		Contact(void);
		~Contact(void);

		// Accessors
		int			GetIndex(void) const;
		void		SetIndex(int v);
		std::string GetFirstName(void) const;
		void		SetFirstName(std::string v);
		std::string GetLastName(void) const;
		void		SetLastName(std::string v);
		std::string GetNickName(void) const;
		void		SetNickName(std::string v);
		std::string GetPhoneNumber(void) const;
		void		SetPhoneNumber(std::string v);
		std::string GetDarkSecret(void) const;
		void		SetDarkSecret(std::string v);
		
	private:
		int			_index;
		std::string _first;
		std::string _last;
		std::string _nick;
		std::string _phone;
		std::string _secret;
};

#endif