/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/29 15:08:57 by sada-sil          #+#    #+#             */
/*   Updated: 2023/12/12 14:07:49 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../fixed.h"

int main( void )
{
	std::cout << std::endl << YELLOW << "Let's first create 3 fixed point numbers." << RESET << std::endl;
	Fixed a;
	Fixed b( a );
	Fixed c;
	
	std::cout << std::endl << YELLOW << "Let's try to call the copy assignment operator too." << RESET << std::endl;
	c = b;

	std::cout << std::endl << YELLOW << "Now let's call the GetRawBits for all the fixed point numbers." << RESET << std::endl;
	std::cout << a.GetRawBits() << std::endl; std::cout
		<< b.GetRawBits() << std::endl; std::cout << c.GetRawBits() << std::endl;
	std::cout << std::endl;
	return 0; 
}