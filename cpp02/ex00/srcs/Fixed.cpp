/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Fixed.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/29 15:10:40 by sada-sil          #+#    #+#             */
/*   Updated: 2023/12/13 10:57:15 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../fixed.h"

// Default constructor
Fixed::Fixed(void)
{
    std::cout << "Fixed default constructor called" << std::endl;
	_value = 0;
    return ;
}

// Copy constructor
Fixed::Fixed(const Fixed &other): _value(other._value)
{
    std::cout << "Fixed copy constructor called" << std::endl;
    return ;
}

// Assignment operator overload
Fixed &Fixed::operator=(const Fixed &other)
{
    std::cout << "Fixed copy Assignment operator called" << std::endl;
    this->_value = other._value;
    return (*this);
}

// Destructor
Fixed::~Fixed(void)
{
    std::cout << "Fixed destructor called" << std::endl;
    return ;
}

int	Fixed::GetRawBits(void) const
{
	std::cout << "GetRawBits called" << std::endl;
	return (_value);
}

void	Fixed::SetRawBits(const int raw)
{
	std::cout << "SetRawBits called" << std::endl;
	_value = raw;
	return ;
}
