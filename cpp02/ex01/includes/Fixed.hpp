/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Fixed.hpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/29 15:10:45 by sada-sil          #+#    #+#             */
/*   Updated: 2023/12/13 13:05:24 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FIXED_HPP
# define FIXED_HPP
# include "../fixed.h"

class Fixed
{
    public:
        Fixed(void);
        Fixed(const Fixed& other);
		Fixed(const int val);
		Fixed(const float val);
        Fixed &operator=(const Fixed &other);
        ~Fixed();
		int	GetRawBits(void) const;
		void SetRawBits(int const raw);
		float ToFloat(void) const;
		int ToInt(void) const;

	private:
		int					_value;
		const static int	_bits = 8;
		
};

std::ostream & operator<<(std::ostream & o, const Fixed &rhs);

#endif

