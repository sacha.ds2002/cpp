/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Fixed.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/29 15:10:40 by sada-sil          #+#    #+#             */
/*   Updated: 2023/12/13 13:13:13 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../fixed.h"

// Default constructor
Fixed::Fixed(void)
{
    std::cout << WHITE << "Fixed default constructor called" << RESET << std::endl;
	_value = 0;
    return ;
}

// Copy constructor
Fixed::Fixed(const int n)
{
	int	val = n;
	
	std::cout << BLUE << "Fixed int constructor called" << RESET << std::endl;
	for (int i = this->_bits; i > 0; i--)
		val *= 2;
	this->_value = val;
	return ;
}

// Copy constructor
Fixed::Fixed(const float val)
{
	float f = val;
	
	std::cout << MAGENTA << "Fixed float constructor called" << RESET << std::endl;
	for (int i = this->_bits; i > 0; i--)
		f *= 2;
	this->_value = roundf(f);
	return ;
}

// Copy constructor
Fixed::Fixed(const Fixed &other): _value(other._value)
{
    std::cout << WHITE << "Fixed copy constructor called" << RESET << std::endl;
    return ;
}

// Assignment operator overload
Fixed &Fixed::operator=(const Fixed &other)
{
    std::cout << WHITE << "Fixed copy assignment operator called" << RESET << std::endl;
    this->_value = other._value;
    return (*this);
}

// Destructor
Fixed::~Fixed(void)
{
    std::cout << RED << "Fixed destructor called" << RESET << std::endl;
    return ;
}

int	Fixed::GetRawBits(void) const
{
	return (_value);
}

void	Fixed::SetRawBits(const int raw)
{
	_value = raw;
	return ;
}

int	Fixed::ToInt(void) const
{
	int val = this->_value;
	for (int i = this->_bits; i > 0; i--)
		val /= 2;
	return (val);
}

float	Fixed::ToFloat(void) const
{
	float val = this->_value;
	for (int i = this->_bits; i > 0; i--)
		val /= 2;
	return (val);
}

std::ostream & operator<<(std::ostream & o, const Fixed &rhs)
{
	o << rhs.ToFloat();
	return o;
}
