/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Fixed.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/29 15:10:40 by sada-sil          #+#    #+#             */
/*   Updated: 2023/12/13 15:09:04 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../fixed.h"

// Default constructor
Fixed::Fixed(void)
{
	std::cout << WHITE << "Fixed default constructor called" << RESET << std::endl;
	_value = 0;
    return ;
}

// Destructor
Fixed::~Fixed(void)
{
	std::cout << RED << "Fixed destructor called" << RESET << std::endl;
    return ;
}

// Copy constructor for int conversion
Fixed::Fixed(const int n)
{
	std::cout << WHITE << "Fixed int constructor called" << RESET << std::endl;
	int	val = n;
	for (int i = this->_bits; i > 0; i--)
		val *= 2;
	this->_value = val;
	return ;
}

// Copy constructor for float conversion
Fixed::Fixed(const float val)
{
	std::cout << WHITE << "Fixed float constructor called" << RESET << std::endl;
	float f = val;
	
	for (int i = this->_bits; i > 0; i--)
		f *= 2;
	this->_value = roundf(f);
	return ;
}

// Copy constructor
Fixed::Fixed(const Fixed &other): _value(other._value)
{
	std::cout << WHITE << "Fixed copy constructor called" << RESET << std::endl;
    return ;
}

// Assignment operator overload
Fixed &Fixed::operator=(const Fixed &other)
{
    // std::cout << "Copy Assignment operator called" << std::endl;
    this->_value = other._value;
    return (*this);
}

// Addition operator overload
Fixed Fixed::operator+(const Fixed &other)
{
    //std::cout << "+ operator called" << std::endl;
    return (Fixed(this->ToFloat() + other.ToFloat()));
}

// Substraction operator overload
Fixed Fixed::operator-(const Fixed &other)
{
    //std::cout << "- operator called" << std::endl;
    return (Fixed(this->ToFloat() - other.ToFloat()));
}

// Multiplication operator overload
Fixed Fixed::operator*(const Fixed &other)
{
    std::cout << "* operator called" << std::endl;
    return (Fixed(this->ToFloat() * other.ToFloat()));
}

// Division operator overload
Fixed Fixed::operator/(const Fixed &other)
{
    //std::cout << "/ operator called" << std::endl;
    return (Fixed(this->ToFloat() / other.ToFloat()));
}

// prefix ++ operator overload
Fixed &Fixed::operator++()
{
    // std::cout << "prefix ++ operator called" << std::endl;
    ++_value;
    return *this;
}

// Postfix ++ operator overload
Fixed Fixed::operator++(int)
{
    // std::cout << "postfix ++ operator called" << std::endl;
    Fixed temp(*this);
	++_value;
	return temp;
}

// Prefix -- operator overload
Fixed &Fixed::operator--()
{
    // std::cout << "prefix -- operator called" << std::endl;
    --_value;
    return *this;
}

// Postfix -- operator overload
Fixed Fixed::operator--(int)
{
    // std::cout << "postfix -- operator called" << std::endl;
    Fixed temp(*this);
	--_value;
	return temp;
}

// Greater than operator overload
bool Fixed::operator>(const Fixed &other)
{
    // std::cout << "> operator called" << std::endl;
	if (this->ToFloat() > other.ToFloat())
    	return true;
	else
		return false;
}

// Smaller than operator overload
bool Fixed::operator<(const Fixed &other)
{
    // std::cout << "< operator called" << std::endl;
	if (this->ToFloat() < other.ToFloat())
    	return true;
	else
		return false;
}

// Greater than or equal to operator overload
bool Fixed::operator>=(const Fixed &other)
{
    // std::cout << ">= operator called" << std::endl;
	if (this->ToFloat() >= other.ToFloat())
    	return true;
	else
		return false;
}

// Smaller than or equal to operator overload
bool Fixed::operator<=(const Fixed &other)
{
    // std::cout << "<= operator called" << std::endl;
	if (this->ToFloat() <= other.ToFloat())
    	return true;
	else
		return false;
}

// Equal to operator overload
bool Fixed::operator==(const Fixed &other)
{
    // std::cout << "== operator called" << std::endl;
	if (this->ToFloat() == other.ToFloat())
    	return true;
	else
		return false;
}

// Not equal to operator overload
bool Fixed::operator!=(const Fixed &other)
{
    // std::cout << "!= operator called" << std::endl;
	if (this->ToFloat() != other.ToFloat())
    	return true;
	else
		return false;
}

int	Fixed::GetRawBits(void) const
{
	return (_value);
}

void	Fixed::SetRawBits(const int raw)
{
	_value = raw;
	return ;
}

int	Fixed::ToInt(void) const
{
	int val = this->_value;
	for (int i = this->_bits; i > 0; i--)
		val /= 2;
	return (val);
}

float	Fixed::ToFloat(void) const
{
	if (_bits >= 0)
	{
		float val = static_cast<float>(_value);
		for (int i = this->_bits; i > 0; i--)
			val /= 2;
		return (val);
	}
	return (0.0f);
}

Fixed &Fixed::Min(Fixed &f1, Fixed &f2)
{
	if (f1 < f2)
		return f1;
	return f2;
}

const Fixed &Fixed::Min(const Fixed &f1, const Fixed &f2)
{
	if (f1.ToFloat() < f2.ToFloat())
		return f1;
	return f2;
}

Fixed &Fixed::Max(Fixed &f1, Fixed &f2)
{
	if (f1 > f2)
		return f1;
	else
		return f2;
}

const Fixed &Fixed::Max(const Fixed &f1, const Fixed &f2)
{
	if (f1.ToFloat() > f2.ToFloat())
		return f1;
	else
		return f2;
}

// Stream operator overload
std::ostream & operator<<(std::ostream & o, const Fixed &rhs)
{
	o << rhs.ToFloat();
	return o;
}
