/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/29 15:08:57 by sada-sil          #+#    #+#             */
/*   Updated: 2023/12/13 15:09:31 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../fixed.h"
#include <iostream>

int main(void) 
{
	std::cout << std::endl << YELLOW << "Let's first create 2 Fixed, one without value and one with a value attributed via the float constructor." << RESET << std::endl;
	
	Fixed a;
	Fixed const b(Fixed(5.05f) * Fixed(2));

	std::cout << YELLOW << "The value of b was attributed via the Float constructor" << std::endl
		<< "using a copy multiplication operator between a float and an int Fixed objects." << RESET << std::endl << std::endl;

	std::cout << "a base value:\t" << a << std::endl;
	std::cout << "++a prefix incr:\t" << ++a << std::endl;
	std::cout << "a value:\t" << a << std::endl << std::endl;
	std::cout << "a++ postfix incr:\t" << std::endl << a++ << std::endl;
	std::cout << std::endl << "a final value:\t" << a << std::endl;
	std::cout << "b value:\t" << b << std::endl;
	std::cout << "greatest between a and b :\t" << Fixed::Max(a, b) << std::endl << std::endl;
return 0;
}
