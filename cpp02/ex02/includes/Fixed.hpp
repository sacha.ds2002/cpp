/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Fixed.hpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/29 15:10:45 by sada-sil          #+#    #+#             */
/*   Updated: 2023/12/13 14:42:04 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FIXED_HPP
# define FIXED_HPP
# include "../fixed.h"

class Fixed
{
    public:
        Fixed(void);
        Fixed(const Fixed& other);
		Fixed(const int val);
		Fixed(const float val);
		~Fixed();
		
		Fixed &operator=(const Fixed &other);
		Fixed operator+(const Fixed &other);
		Fixed operator-(const Fixed &other);
		Fixed operator*(const Fixed &other);
		Fixed operator/(const Fixed &other);
		Fixed &operator++();
		Fixed operator++(int);
		Fixed &operator--();
		Fixed operator--(int);

		bool operator>(const Fixed &other);
		bool operator<(const Fixed &other);
		bool operator>=(const Fixed &other);
		bool operator<=(const Fixed &other);
		bool operator==(const Fixed &other);
		bool operator!=(const Fixed &other);
		
		int	GetRawBits(void) const;
		void SetRawBits(int const raw);
		float ToFloat(void) const;
		int ToInt(void) const;

		static 			Fixed &Min(Fixed &f1, Fixed &f2);
		static const 	Fixed &Min(const Fixed &f1, const Fixed &f2);
		static 			Fixed &Max(Fixed &f1, Fixed &f2);
		static const 	Fixed &Max(const Fixed &f1, const Fixed &f2);
	private:
		int					_value;
		const static int	_bits = 8;
};

std::ostream & operator<<(std::ostream & o, const Fixed &rhs);

#endif

