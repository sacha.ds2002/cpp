/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Intern.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/02/28 12:02:34 by sada-sil          #+#    #+#             */
/*   Updated: 2024/05/17 13:10:08 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef INTERN_HPP
# define INTERN_HPP
# include "../bureaucrat.h"

class Intern
{
    public:
		class WrongFormName : public std::exception
		{
			virtual const char *what() const throw()
			{
				return ("The Form couldn't be found, the name is wrong.");
			}
		};
		
        Intern(void);
        Intern(const Intern& other);
        Intern &operator=(const Intern &other);
        ~Intern();
		
		AForm	*MakeForm(std::string formName, std::string target) const;
};

#endif

