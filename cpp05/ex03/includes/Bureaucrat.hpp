/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Bureaucrat.hpp                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/02/16 12:37:42 by sada-sil          #+#    #+#             */
/*   Updated: 2024/02/27 15:28:03 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef BUREAUCRAT_HPP
# define BUREAUCRAT_HPP
# include "../bureaucrat.h"

class Bureaucrat
{
    public:
		class GradeTooHighException : public std::exception
		{
			virtual const char *what() const throw()
			{
				return ("The grade is too high.");
			}
		};
		class GradeTooLowException : public std::exception
		{
			virtual const char *what() const throw()
			{
				return ("The grade is too low.");
			}
		};
        Bureaucrat(void);
		Bureaucrat(std::string name, int grade);
        Bureaucrat(const Bureaucrat& other);
        Bureaucrat &operator=(const Bureaucrat &other);
        ~Bureaucrat();

		void		SignForm(AForm &f);
		std::string	GetName(void) const;
		int			GetGrade(void) const;
		void		IncrementGrade(int value);
		void		DecrementGrade(int value);
		void		ExecuteForm(AForm const & form);
		
	private:
		std::string const	_name;
		int					_grade;
};

std::ostream & operator<<(std::ostream & o, const Bureaucrat &rhs);

#endif

