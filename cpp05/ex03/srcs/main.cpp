/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/02/16 12:37:02 by sada-sil          #+#    #+#             */
/*   Updated: 2024/05/17 13:10:58 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../bureaucrat.h"

int	main(void)
{
	Bureaucrat b1("Henry", 1);
	Bureaucrat b2("Alex", 150);

	std::cout << CYAN << std::endl << b1 << std::endl << b2 << RESET << std::endl;

	Intern		i1;
	std::cout << std::endl << MAGENTA <<  "bullshit, bullshit" << RESET << std::endl;
	AForm *f1 = i1.MakeForm("bullshit", "bullshit");
	std::cout << std::endl << MAGENTA <<  "shrubbery creation, Garden" << RESET << std::endl;
	AForm *f2 = i1.MakeForm("shrubbery creation", "Garden");
	std::cout << std::endl << MAGENTA <<  "robotomy request, Alex" << RESET << std::endl;
	AForm *f3 = i1.MakeForm("robotomy request", "Alex");
	std::cout << std::endl << MAGENTA <<  "presidential pardon, Henry" << RESET << std::endl;
	AForm *f4 = i1.MakeForm("presidential pardon", "Henry");
	std::cout << std::endl;
	
	// b1.SignForm(*f1); 
	// this will have undefined behaviour since f1 is nullptr and a reference can't be a nullptr
	// this could be managable by using a ptr instead of a ref, but exercises asks for a reference
	
	b1.SignForm(*f2);
	b1.SignForm(*f3);
	b1.SignForm(*f4);

	std::cout << std::endl;
	return 0;
}