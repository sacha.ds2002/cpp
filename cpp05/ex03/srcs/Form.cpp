/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Form.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/02/26 10:40:27 by sada-sil          #+#    #+#             */
/*   Updated: 2024/02/28 13:54:54 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../bureaucrat.h"

// Default constructor
AForm::AForm(void) : _name("Default"), _signed(false), _signG(20), _execG(5)
{
    return ;
}

// Naming constructor
AForm::AForm(std::string name) : _name(name), _signed(false), _signG(20), _execG(5)
{
    return ;
}

// Naming constructor
AForm::AForm(std::string name, int signg, int execg) : _name(name), _signed(false),
	_signG(signg), _execG(execg)
{
	try
	{
		if (signg > 150 || execg > 150)
			throw AForm::GradeTooLowException();
		else if (signg < 1 || execg < 1)
			throw AForm::GradeTooHighException();
	}
	catch(const std::exception& e)
	{
		std::cerr << e.what() << '\n';
	}
    return ;
}

// Copy constructor
AForm::AForm(const AForm &other) : _name(other.GetName()), _signed(other.GetSigned()), 
	_signG(other.GetSignGrade()), _execG(other.GetSignGrade())
{
    std::cout << "Form's copy constructor called" << std::endl;
    (void) other;
    return ;
}

// Assignment operator overload
AForm &AForm::operator=(const AForm &other)
{
    std::cout << "Form's assignment operator called" << std::endl;
	_signed = other.GetSigned();
    return (*this);
}

// Destructor
AForm::~AForm(void)
{
    return ;
}

std::string AForm::GetName(void) const
{
	return this->_name;
}

bool AForm::GetSigned(void) const
{
	return this->_signed;
}
int	AForm::GetSignGrade(void) const
{
	return this->_signG;
}

int AForm::GetExecGrade(void) const
{
	return this->_execG;
}

bool	AForm::BeSigned(Bureaucrat *b)
{
	try
	{
		if (b->GetGrade() > this->GetSignGrade())
			throw AForm::GradeTooLowException();
		this->_signed = true;
		return (true);
	}
	catch(const std::exception& e)
	{
		std::cerr << e.what() << '\n';
		return (false);
	}
	return (false);
}

bool	AForm::CheckRequirements(Bureaucrat const &executor) const
{
	try
	{
		if (executor.GetGrade() > this->GetExecGrade())
			throw AForm::GradeTooLowException();
		else if (!this->GetSigned())
			throw AForm::FormUnsigned();
		return true;
	}
	catch (const std::exception& e)
	{
		std::cerr << RED << e.what() << RESET << '\n';
		return false;
	}
}

// Stream operator overload
std::ostream & operator<<(std::ostream & o, const AForm &rhs)
{
	o << "Form " << rhs.GetName() << " is " << (rhs.GetSigned() ? "signed" : "not signed") << ".\n"
		<< "Sign grade: " << rhs.GetSignGrade() << ".\n"
		<< "Exec grade: " << rhs.GetExecGrade() << ".\n";
	return o;
}
