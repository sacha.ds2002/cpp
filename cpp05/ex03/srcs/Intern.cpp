/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Intern.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/02/28 12:02:33 by sada-sil          #+#    #+#             */
/*   Updated: 2024/02/28 13:55:51 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../bureaucrat.h"

// Default constructor
Intern::Intern(void)
{
    std::cout << "Intern Default constructor" << std::endl;
    return ;
}

// Copy constructor
Intern::Intern(const Intern &other)
{
    std::cout << "Intern Copy constructor" << std::endl;
    (void) other;
    return ;
}

// Assignment operator overload
Intern &Intern::operator=(const Intern &other)
{
    std::cout << "Intern Assignment operator" << std::endl;
    (void) other;
    return (*this);
}

// Destructor
Intern::~Intern(void)
{
    std::cout << "Intern Destructor" << std::endl;
    return ;
}

AForm	*Intern::MakeForm(std::string formName, std::string target) const
{
	std::string fNames[3] = {"shrubbery creation","robotomy request","presidential pardon"};
	AForm *forms[3] = 
	{
		new ShrubberyCreationForm(), 
		new RobotomyRequestForm(), 
		new PresidentialPardonForm()
	};
	
	AForm *createdForm = nullptr;
	try
	{
		for (size_t i = 0; i < (sizeof(fNames) / sizeof(fNames[0])); i++)
		{
			if (formName == fNames[i])
				createdForm = forms[i];
			else
				delete forms[i];
		}
		if (createdForm == nullptr)
			throw Intern::WrongFormName();	
	}
	catch(const std::exception& e)
	{
		std::cerr << RED << e.what() << '\n' << RESET;
		return NULL;
	}
	std::cout << YELLOW << "Intern created " << formName << "." << RESET << std::endl;
	return createdForm;
}
