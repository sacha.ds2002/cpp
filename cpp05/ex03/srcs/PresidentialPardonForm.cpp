/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   PresidentialPardonForm.cpp                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/02/27 13:31:22 by sada-sil          #+#    #+#             */
/*   Updated: 2024/02/28 13:54:36 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../bureaucrat.h"

// Default constructor
PresidentialPardonForm::PresidentialPardonForm(void) : AForm("Presidential", 25, 5)
{
    std::cout << "Presidential Default constructor" << std::endl;
	_target = "default";
    return ;
}

// Target constructor
PresidentialPardonForm::PresidentialPardonForm(std::string target) : AForm("Presidential", 25, 5), _target(target)
{
    std::cout << "Presidential Target constructor" << std::endl;
    return ;
}

// Copy constructor
PresidentialPardonForm::PresidentialPardonForm(const PresidentialPardonForm &other)
{
    std::cout << "Copy constructor called" << std::endl;
    (void) other;
    return ;
}

// Assignment operator overload
PresidentialPardonForm &PresidentialPardonForm::operator=(const PresidentialPardonForm &other)
{
    std::cout << "Assignment operator called" << std::endl;
    (void) other;
    return (*this);
}

// Destructor
PresidentialPardonForm::~PresidentialPardonForm(void)
{
    std::cout << "PresidentialPardonForm destructor called" << std::endl;
    return ;
}

bool	PresidentialPardonForm::Execute(Bureaucrat const &executor) const
{
	if (this->CheckRequirements(executor))
	{
		std::cout << BLUE << this->_target << " has been pardonned by Zaphod Beeblebrox"
			<< RESET << std::endl;
		return true;
    }
	return false;
}
