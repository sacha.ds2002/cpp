/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/02/16 12:37:02 by sada-sil          #+#    #+#             */
/*   Updated: 2024/05/17 11:04:50 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../bureaucrat.h"

int	main(void)
{
	std::cout << std::endl;
	
	Bureaucrat b1("Henry", 1);
	Bureaucrat b2("Alex", 150);
	Bureaucrat b3("George", 151);
	Bureaucrat b4("James", 0);

	b1.IncrementGrade(1);
	b2.DecrementGrade(1);

	std::cout << b1 << b2 << b3 << b4;
	
	return 0;
}