/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Form.hpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/02/26 10:40:13 by sada-sil          #+#    #+#             */
/*   Updated: 2024/02/27 15:26:48 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FORM_HPP
# define FORM_HPP
# include "../bureaucrat.h"

class Bureaucrat;

class AForm
{
    public:
		class GradeTooHighException : public std::exception
		{
			virtual const char *what() const throw()
			{
				return ("The grade is too high.");
			}
		};
		
		class GradeTooLowException : public std::exception
		{
			virtual const char *what() const throw()
			{
				return ("The grade is too low.");
			}
		};

		class FormUnsigned : public std::exception
		{
			virtual const char *what() const throw()
			{
				return ("The Form isn't signed.");
			}
		};

        AForm(void);
		AForm(std::string name);
		AForm(std::string name, int signg, int execg);
        AForm(const AForm& other);
        AForm &operator=(const AForm &other);
        ~AForm();

		virtual bool	Execute(Bureaucrat const &executor) const = 0;
		bool			BeSigned(Bureaucrat *b);
		std::string		GetName(void) const;
		bool			GetSigned(void) const;
		int				GetSignGrade(void) const;
		int				GetExecGrade(void) const;

	protected:
		bool			CheckRequirements(Bureaucrat const &executor) const;

	private:
		std::string const	_name;
		bool				_signed;
		const int			_signG;
		const int			_execG;
};

std::ostream & operator<<(std::ostream & o, const AForm &rhs);

#endif

