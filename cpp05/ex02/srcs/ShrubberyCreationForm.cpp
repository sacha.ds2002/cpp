/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ShrubberyCreationForm.cpp                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/02/27 13:31:14 by sada-sil          #+#    #+#             */
/*   Updated: 2024/02/27 15:49:25 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../bureaucrat.h"

// Default constructor
ShrubberyCreationForm::ShrubberyCreationForm(void) : AForm("Shrubbery", 145, 137)
{
    std::cout << "Shrubbery Default constructor" << std::endl;
	_target = "default";
    return ;
}

// Target constructor
ShrubberyCreationForm::ShrubberyCreationForm(std::string target) : AForm("Shrubbery", 145, 137), _target(target)
{
    std::cout << "Shrubbery Target constructor" << std::endl;
    return ;
}

// Copy constructor
ShrubberyCreationForm::ShrubberyCreationForm(const ShrubberyCreationForm &other)
{
    std::cout << "Copy constructor called" << std::endl;
    (void) other;
    return ;
}

// Assignment operator overload
ShrubberyCreationForm &ShrubberyCreationForm::operator=(const ShrubberyCreationForm &other)
{
    std::cout << "Assignment operator called" << std::endl;
    (void) other;
    return (*this);
}

// Destructor
ShrubberyCreationForm::~ShrubberyCreationForm(void)
{
    std::cout << "Destructor called" << std::endl;
    return ;
}

bool	ShrubberyCreationForm::Execute(Bureaucrat const &executor) const
{
	if (this->CheckRequirements(executor))
	{
		std::ofstream outputFile(this->_target + "_shrubbery.txt");
		if (outputFile.is_open())
		{
			outputFile
			<< "          &&& &&  & &&\n"
			<< "      && &\\/&\\|& ()|/ @, &&\n"
			<< "      &\\/(/&/&||/& /_/)_&/_&\n"
			<< "   &() &\\/&|()|/&\\/ '%\" & ()\n"
			<< "  &_\\_&&_\\ |& |&&/&__%_/_& &&\n"
			<< "&&   && & &| &| /& & % ()& /&&\n"
			<< " ()&_---()&\\&\\|&&-&&--%---()~\n"
			<< "     &&     \\|||\n"
			<< "             |||\n"
			<< "             |||\n"
			<< "             |||\n"
			<< "       , -=-~  .-^- _\n"
			<< "              `\n"
			<< std::endl;
			outputFile.close();
			return true;
		}
    }
	return false;
}
