/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   RobotomyRequestForm.cpp                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/02/27 13:31:27 by sada-sil          #+#    #+#             */
/*   Updated: 2024/02/27 15:37:05 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../bureaucrat.h"

// Default constructor
RobotomyRequestForm::RobotomyRequestForm(void) : AForm("Robotomy", 72, 45)
{
    std::cout << "Robotomy Default constructor" << std::endl;
	_target = "default";
    return ;
}

// Target constructor
RobotomyRequestForm::RobotomyRequestForm(std::string target) : AForm("Robotomy", 72, 45), _target(target)
{
    std::cout << "Robotomy Target constructor" << std::endl;
    return ;
}

// Copy constructor
RobotomyRequestForm::RobotomyRequestForm(const RobotomyRequestForm &other)
{
    std::cout << "Copy constructor called" << std::endl;
    (void) other;
    return ;
}

// Assignment operator overload
RobotomyRequestForm &RobotomyRequestForm::operator=(const RobotomyRequestForm &other)
{
    std::cout << "Assignment operator called" << std::endl;
    (void) other;
    return (*this);
}

// Destructor
RobotomyRequestForm::~RobotomyRequestForm(void)
{
    std::cout << "Destructor called" << std::endl;
    return ;
}

bool	RobotomyRequestForm::Execute(Bureaucrat const &executor) const
{
	if (this->CheckRequirements(executor))
	{
		std::cout << RED << "*Drill Drill*" << std::endl;
		std::random_device rng;
		std::uniform_int_distribution<int> dist(1, 2);
		if (dist(rng) == 1)
			std::cout << this->_target << " has been robotomized" << RESET << std::endl;
		else
			std::cout << "Robotomy failed." << RESET << std::endl;
		return true;
    }
	return false;
}
