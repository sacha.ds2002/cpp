/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/02/16 12:37:02 by sada-sil          #+#    #+#             */
/*   Updated: 2024/02/27 15:50:06 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../bureaucrat.h"

int	main(void)
{
	Bureaucrat b1("Henry", 1);
	Bureaucrat b2("Alex", 150);

	std::cout << CYAN << std::endl << b1 << std::endl << b2 << RESET << std::endl;

	ShrubberyCreationForm s("Home");
	RobotomyRequestForm r("Alex");
	PresidentialPardonForm p("Henry");
	
	std::cout << CYAN << std::endl << s << std::endl << r << std::endl << p << RESET << std::endl;

	b1.ExecuteForm(s);
	b1.SignForm(s);
	b1.ExecuteForm(s);
	b2.ExecuteForm(s);

	std::cout << std::endl;

	b1.ExecuteForm(r);
	b1.SignForm(r);
	b1.ExecuteForm(r);
	b2.ExecuteForm(r);

	std::cout << std::endl;

	b1.ExecuteForm(p);
	b1.SignForm(p);
	b1.ExecuteForm(p);
	b2.ExecuteForm(p);
	
	std::cout << std::endl;
	
	return 0;
}