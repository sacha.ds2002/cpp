/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Form.hpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/02/26 10:40:13 by sada-sil          #+#    #+#             */
/*   Updated: 2024/02/26 14:48:18 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FORM_HPP
# define FORM_HPP
# include "../bureaucrat.h"

class Bureaucrat;

class Form
{
    public:
		class GradeTooHighException : public std::exception
		{
			virtual const char *what() const throw()
			{
				return ("The grade is too high.");
			}
		};
		
		class GradeTooLowException : public std::exception
		{
			virtual const char *what() const throw()
			{
				return ("The grade is too low.");
			}
		};

        Form(void);
		Form(std::string name);
		Form(std::string name, int signg, int execg);
        Form(const Form& other);
        Form &operator=(const Form &other);
        ~Form();

		bool		BeSigned(Bureaucrat *b);
		std::string	GetName(void) const;
		bool		GetSigned(void) const;
		int			GetSignGrade(void) const;
		int			GetExecGrade(void) const;

	private:
		std::string const	_name;
		bool				_signed;
		const int			_signG;
		const int			_execG;
};

std::ostream & operator<<(std::ostream & o, const Form &rhs);

#endif

