/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Bureaucrat.cpp                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/02/16 12:37:28 by sada-sil          #+#    #+#             */
/*   Updated: 2024/05/17 11:36:52 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../bureaucrat.h"

// Default constructor
Bureaucrat::Bureaucrat(void) : _name("blank")
{
    std::cout << "Default constructor called" << std::endl;
	_grade = 150;
    return ;
}

// Name constructor
Bureaucrat::Bureaucrat(std::string name, int grade) : _name(name)
{
    std::cout << MAGENTA << name << "'s naming constructor called with grade value: "
		<< grade << RESET << std::endl;
	try
	{
		if (grade > 150)
			throw Bureaucrat::GradeTooLowException();
		else if (grade < 1)
			throw Bureaucrat::GradeTooHighException();
		_grade = grade;
	}
	catch(const std::exception& e)
	{
		std::cerr << RED << e.what() << RESET << '\n';
		this->_grade = 150;
	}
    return ;
}

// Copy constructor
Bureaucrat::Bureaucrat(const Bureaucrat &other) : _name(other.GetName()), _grade(other.GetGrade())
{
    std::cout << "Copy constructor called" << std::endl;
	(void) other;
    return ;
}

// Assignment operator overload
Bureaucrat &Bureaucrat::operator=(const Bureaucrat &other)
{
    std::cout << "Assignment operator called" << std::endl;
	// Can't set name since _name is constant
	this->_grade = other.GetGrade();
    return *this;
}

// Destructor
Bureaucrat::~Bureaucrat(void)
{
    std::cout << "Destructor called" << std::endl;
    return ;
}

std::string	Bureaucrat::GetName(void) const
{
	return this->_name;
}

int	Bureaucrat::GetGrade(void) const
{
	return this->_grade;
}

void	Bureaucrat::IncrementGrade(int value)
{
	std::cout << GREEN << this->_name << "'s IncrementGrade function called with value: "
		<< value << RESET << std::endl;
	try
	{
		if ((this->_grade - value) < 1)
			throw Bureaucrat::GradeTooHighException();
		this->_grade -= value;
	}
	catch(const std::exception& e)
	{
		std::cerr << RED << e.what() << RESET << '\n';
	}	
}

void	Bureaucrat::DecrementGrade(int value)
{
	std::cout << BLUE << this->_name << "'s DecrementGrade function called with value: "
		<< value << RESET << std::endl;
	try
	{
		if ((this->_grade + value) > 150)
			throw Bureaucrat::GradeTooLowException();
		this->_grade += value;
	}
	catch(const std::exception& e)
	{
		std::cerr << RED << e.what() << RESET << '\n';
	}	
}

void		Bureaucrat::SignForm(Form &f)
{
	if (!f.GetSigned())
	{
		if (f.BeSigned(this))
			std::cout << GREEN << this->_name << " signed " << f.GetName() << "." << RESET << std::endl;
		else
			std::cout << RED << this->_name << " couldn't sign " << f.GetName() 
				<< " because the Bureaucrat's grade is too low." << RESET << std::endl;
	}
	else
	{
		std::cout << RED << this->_name << " couldn't sign " << f.GetName() 
				<< " because the Form was already signed." << RESET << std::endl;
	}
}

// Stream operator overload
std::ostream & operator<<(std::ostream & o, const Bureaucrat &rhs)
{
	o << rhs.GetName() << ",\tbureaucrat grade " << rhs.GetGrade() << ".\n";
	return o;
}