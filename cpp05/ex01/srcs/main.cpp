/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/02/16 12:37:02 by sada-sil          #+#    #+#             */
/*   Updated: 2024/02/26 14:49:44 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../bureaucrat.h"

int	main(void)
{
	Bureaucrat b1("Henry", 1);
	Bureaucrat b2("Alex", 150);
	Bureaucrat b3("George", 151);
	Bureaucrat b4("James", 0);

	b1.IncrementGrade(1);
	b2.DecrementGrade(1);

	std::cout << b1 << b2 << b3 << b4;

	Form f1("A1", 20, 10);
	b1.SignForm(f1);
	b1.SignForm(f1);

	Form f2("A2", 1, 1);
	b2.SignForm(f2);
	
	std::cout << BLUE << std::endl << f1 << std::endl << f2 << RESET << std::endl;
	
	return 0;
}