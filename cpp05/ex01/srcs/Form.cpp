/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Form.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/02/26 10:40:27 by sada-sil          #+#    #+#             */
/*   Updated: 2024/05/17 11:32:54 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../bureaucrat.h"

// Default constructor
Form::Form(void) : _name("Default"), _signed(false), _signG(20), _execG(5)
{
    std::cout << "Form's default constructor called" << std::endl;
    return ;
}

// Naming constructor
Form::Form(std::string name) : _name(name), _signed(false), _signG(20), _execG(5)
{
    std::cout << "Form's naming constructor called" << std::endl;
    return ;
}

// Naming constructor
Form::Form(std::string name, int signg, int execg) : _name(name), _signed(false),
	_signG(signg), _execG(execg)
{
    std::cout << "Form's params constructor called" << std::endl;
	try
	{
		if (signg > 150 || execg > 150)
			throw Form::GradeTooLowException();
		else if (signg < 1 || execg < 1)
			throw Form::GradeTooHighException();
	}
	catch(const std::exception& e)
	{
		std::cerr << e.what() << '\n';
	}
    return ;
}

// Copy constructor
Form::Form(const Form &other) : _name(other.GetName()), _signed(other.GetSigned()), 
	_signG(other.GetSignGrade()), _execG(other.GetExecGrade())
{
    std::cout << "Form's copy constructor called" << std::endl;
    (void) other;
    return ;
}

// Assignment operator overload
Form &Form::operator=(const Form &other)
{
    std::cout << "Form's assignment operator called" << std::endl;
	_signed = other.GetSigned();
    return (*this);
}

// Destructor
Form::~Form(void)
{
    std::cout << "Form's destructor called" << std::endl;
    return ;
}

std::string Form::GetName(void) const
{
	return this->_name;
}

bool Form::GetSigned(void) const
{
	return this->_signed;
}
int	Form::GetSignGrade(void) const
{
	return this->_signG;
}

int Form::GetExecGrade(void) const
{
	return this->_execG;
}

bool	Form::BeSigned(Bureaucrat *b)
{
	try
	{
		if (b->GetGrade() > this->GetSignGrade())
			throw Form::GradeTooLowException();
		this->_signed = true;
		return (true);
	}
	catch(const std::exception& e)
	{
		std::cerr << e.what() << '\n';
		return (false);
	}
	return (false);
}

// Stream operator overload
std::ostream & operator<<(std::ostream & o, const Form &rhs)
{
	o << "Form " << rhs.GetName() << " is " << (rhs.GetSigned() ? "signed" : "not signed") << ".\n"
		<< "Sign grade: " << rhs.GetSignGrade() << ".\n"
		<< "Exec grade: " << rhs.GetExecGrade() << ".\n";
	return o;
}
