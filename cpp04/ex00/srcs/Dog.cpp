/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Dog.cpp                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/20 10:02:58 by sada-sil          #+#    #+#             */
/*   Updated: 2023/12/20 11:40:25 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../animal.h"

// Default constructor
Dog::Dog(void)
{
    std::cout << RED << "Dog default constructor called" << std::endl;
	_type = "Dog";
    return ;
}

// Copy constructor
Dog::Dog(const Dog &other)
{
    std::cout << RED << "Dog copy constructor called" << std::endl;
    _type = other.GetType();
    return ;
}

// Assignment operator overload
Dog &Dog::operator=(const Dog &other)
{
    std::cout << RED << "Dog assignment operator called" << std::endl;
    _type = other.GetType();
    return (*this);
}

// Destructor
Dog::~Dog(void)
{
    std::cout << RED << "Dog destructor called" << std::endl;
    return ;
}

void Dog::MakeNoise() const
{
	std::cout << RED << "Waf waf" << RESET << std::endl;
}
