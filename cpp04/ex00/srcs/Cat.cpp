/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Cat.cpp                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/20 10:03:27 by sada-sil          #+#    #+#             */
/*   Updated: 2023/12/20 11:40:22 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../animal.h"

// Default constructor
Cat::Cat(void)
{
    std::cout << GREEN << "Cat default constructor called" << std::endl;
	_type = "Cat";
    return ;
}

// Copy constructor
Cat::Cat(const Cat &other)
{
    std::cout << GREEN << "Cat copy constructor called" << std::endl;
    _type = other.GetType();
    return ;
}

// Assignment operator overload
Cat &Cat::operator=(const Cat &other)
{
    std::cout << GREEN << "Cat assignment operator called" << std::endl;
    _type = other.GetType();
    return (*this);
}

// Destructor
Cat::~Cat(void)
{
    std::cout << GREEN << "Cat destructor called" << std::endl;
    return ;
}

void Cat::MakeNoise() const
{
	std::cout << GREEN << "Meow" << RESET << std::endl;
}
