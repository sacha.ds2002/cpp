/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   WrongAnimal.cpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/20 11:31:23 by sada-sil          #+#    #+#             */
/*   Updated: 2023/12/20 11:52:42 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../animal.h"

// Default constructor
WrongAnimal::WrongAnimal(void)
{
    std::cout << RESET << "WrongAnimal default constructor called" << std::endl;
    return ;
}

// Copy constructor
WrongAnimal::WrongAnimal(const WrongAnimal &other)
{
    std::cout << RESET << "WrongAnimal copy constructor called" << std::endl;
    (void) other;
    return ;
}

// Assignment operator overload
WrongAnimal &WrongAnimal::operator=(const WrongAnimal &other)
{
    std::cout << RESET << "WrongAnimal assignment operator called" << std::endl;
    (void) other;
    return (*this);
}

// Destructor
WrongAnimal::~WrongAnimal(void)
{
    std::cout << RESET << "WrongAnimal destructor called" << std::endl;
    return ;
}

void	WrongAnimal::MakeNoise() const
{
	std::cout << BLUE << "Weird animal noise?" << RESET << std::endl;
}
