/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   WrongCat.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/20 11:32:32 by sada-sil          #+#    #+#             */
/*   Updated: 2023/12/20 11:51:33 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../animal.h"

// Default constructor
WrongCat::WrongCat(void)
{
    std::cout << GREEN << "WrongCat default constructor called" << std::endl;
    return ;
}

// Copy constructor
WrongCat::WrongCat(const WrongCat &other)
{
    std::cout << GREEN << "WrongCat copy constructor called" << std::endl;
    (void) other;
    return ;
}

// Assignment operator overload
WrongCat &WrongCat::operator=(const WrongCat &other)
{
    std::cout << GREEN << "WrongCat assignment operator called" << std::endl;
    (void) other;
    return (*this);
}

// Destructor
WrongCat::~WrongCat(void)
{
    std::cout << GREEN << "WrongCat destructor called" << std::endl;
    return ;
}
