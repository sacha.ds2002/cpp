/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Animal.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/20 09:57:31 by sada-sil          #+#    #+#             */
/*   Updated: 2023/12/20 11:40:31 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../animal.h"

// Default constructor
Animal::Animal(void)
{
    std::cout << RESET  << "Animal default constructor called" << std::endl;
    return ;
}

// Copy constructor
Animal::Animal(const Animal &other)
{
    std::cout << RESET  << "Animal copy constructor called" << std::endl;
    _type = other._type;
    return ;
}

// Assignment operator overload
Animal &Animal::operator=(const Animal &other)
{
    std::cout << RESET  << "Animal assignment operator called" << std::endl;
    _type = other._type;
    return (*this);
}

// Destructor
Animal::~Animal(void)
{
    std::cout << RESET  << "Animal destructor called" << std::endl;
    return ;
}

std::string Animal::GetType() const
{
	if (_type.empty())
		return ("unknown");
	return (_type);	
}

void Animal::MakeNoise() const
{
	std::cout << RESET  << "..." << std::endl;
}
