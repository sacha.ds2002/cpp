/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/20 09:58:14 by sada-sil          #+#    #+#             */
/*   Updated: 2023/12/20 11:54:08 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../animal.h"

void SimpleTests()
{
	Animal animal = Animal();
	Cat cat = Cat();
	Dog dog = Dog();
	
	std::cout << std::endl << YELLOW << "MakeNoise functions:" << RESET << std::endl;

	animal.MakeNoise();
	cat.MakeNoise();
	dog.MakeNoise();

	std::cout << std::endl << YELLOW << "Let's check that their type is correct" << RESET << std::endl;
	
	std::cout << WHITE << "animal's type: " << animal.GetType() << std::endl;
	std::cout << GREEN << "cat's type: " << cat.GetType() << std::endl;
	std::cout << RED << "dog's type: " << dog.GetType() << std::endl << std::endl;
}

void PolyTests()
{
	std::cout << YELLOW << "Let's create an animal with the Cat's constructor" << RESET << std::endl;
	Animal animal = Cat();
	std::cout << std::endl << YELLOW << "The animal's MakeNoise and type:" << RESET << std::endl;
	animal.MakeNoise();
	std::cout << WHITE << "animal's type: " << animal.GetType() << std::endl << std::endl;
}

void WrongTests()
{
	std::cout << YELLOW << "Let's create an WrongAnimal and a WrongCat" << RESET << std::endl;
	WrongAnimal wAnimal = WrongAnimal();
	WrongCat wCat = WrongCat();

	std::cout << std::endl << YELLOW << "Their MakeNoise functions should be similar" << std::endl
		<< "because it hasn't been overwritten by the WrongCat" << RESET << std::endl;
	wAnimal.MakeNoise();
	wCat.MakeNoise();
	std::cout << std::endl;
}

int	main(void)
{
	std::cout << std::endl << MAGENTA << "----- SIMPLE TESTS -----" << RESET << std::endl;
 	SimpleTests();
	
	std::cout << std::endl << MAGENTA << "----- POLYMORPHISM TESTS -----" << RESET << std::endl;
	PolyTests();

	std::cout << std::endl << MAGENTA << "----- WRONG CLASSES TESTS -----" << RESET << std::endl;
	WrongTests();
	
	return 0;
}