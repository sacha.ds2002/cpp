/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Animal.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/20 09:56:25 by sada-sil          #+#    #+#             */
/*   Updated: 2023/12/20 11:40:00 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ANIMAL_HPP
# define ANIMAL_HPP
# include "../animal.h"

class Animal
{
    public:
        Animal(void);
        Animal(const Animal& other);
        Animal &operator=(const Animal &other);
        ~Animal();

		std::string GetType() const;

		virtual void MakeNoise() const;

	protected:
		std::string _type;
};

#endif

