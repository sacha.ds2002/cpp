/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Character.hpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/21 15:56:38 by sada-sil          #+#    #+#             */
/*   Updated: 2023/12/21 16:20:33 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CHARACTER_HPP
# define CHARACTER_HPP
# include "../materia.h"

class Character : public ICharacter
{
    public:
        Character(void);
		Character(std::string name);
        Character(const Character& other);
        Character &operator=(const Character &other);
        ~Character();

		virtual std::string const & GetName() const;
		virtual void Equip(AMateria* m);
		virtual void Unequip(int idx);
		virtual void Use(int idx, ICharacter& target);

	private:
		std::string	_name;
		AMateria	*_inventory[4];
};

#endif

