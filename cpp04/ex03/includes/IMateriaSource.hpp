/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   IMateriaSource.hpp                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/22 11:11:27 by sada-sil          #+#    #+#             */
/*   Updated: 2023/12/22 11:20:30 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef IMATERIASOURCE_HPP
# define IMATERIASOURCE_HPP
# include "../materia.h"

class IMateriaSource
{
	public:
		virtual ~IMateriaSource() {}
		virtual void LearnMateria(AMateria *materia) = 0;
		virtual AMateria* CreateMateria(std::string const & type) = 0;
};

#endif

