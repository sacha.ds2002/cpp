/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Cure.hpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/21 15:56:41 by sada-sil          #+#    #+#             */
/*   Updated: 2023/12/22 11:30:44 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CURE_HPP
# define CURE_HPP
# include "../materia.h"

class Cure : public AMateria
{
    public:
        Cure(void);
        Cure(const Cure& other);
        Cure &operator=(const Cure &other);
        virtual ~Cure();

		virtual AMateria* Clone() const;
		virtual void Use(ICharacter& target);
};

#endif

