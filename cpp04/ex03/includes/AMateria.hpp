/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   AMateria.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/21 15:57:07 by sada-sil          #+#    #+#             */
/*   Updated: 2023/12/22 11:25:57 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef AMATERIA_HPP
# define AMATERIA_HPP
# include "../materia.h"

class ICharacter;

class AMateria
{
    public:
        AMateria(void);
		AMateria(std::string const & type);
        AMateria(const AMateria& other);
        AMateria &operator=(const AMateria &other);
        virtual ~AMateria();
		std::string const & GetType(void) const; //Returns the materia type
		virtual AMateria* Clone(void) const = 0;
		virtual void Use(ICharacter& target);

	protected:
		std::string	_type;
};

#endif

