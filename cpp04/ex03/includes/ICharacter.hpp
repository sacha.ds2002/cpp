/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ICharacter.hpp                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/21 15:56:46 by sada-sil          #+#    #+#             */
/*   Updated: 2023/12/22 11:15:38 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ICHARACTER_HPP
# define ICHARACTER_HPP
# include "../materia.h"

class AMateria;

class ICharacter
{
    public:
		virtual ~ICharacter() {}
		virtual std::string const & GetName() const = 0;
		virtual void Equip(AMateria* m) = 0;
		virtual void Unequip(int idx) = 0;
		virtual void Use(int idx, ICharacter& target) = 0;
};

#endif

