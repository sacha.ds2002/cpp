/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   materia.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/23 14:10:40 by sada-sil          #+#    #+#             */
/*   Updated: 2023/12/22 11:11:42 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MATERIA_H
# define MATERIA_H

# define RESET			"\033[0m"
# define RED			"\033[0;31m"
# define GREEN			"\033[0;32m"
# define YELLOW			"\033[0;33m"
# define BLUE			"\033[0;34m"
# define MAGENTA		"\033[0;35m"
# define CYAN			"\033[0;36m"
# define WHITE			"\033[0;37m"

# include <iostream>
# include <string>
# include <iomanip>

# include "includes/ICharacter.hpp"
# include "includes/AMateria.hpp"
# include "includes/IMateriaSource.hpp"
# include "includes/Character.hpp"
# include "includes/Cure.hpp"
# include "includes/Ice.hpp"
# include "includes/MateriaSource.hpp"

#endif