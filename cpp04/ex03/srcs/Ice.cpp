/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Ice.cpp                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/21 15:57:19 by sada-sil          #+#    #+#             */
/*   Updated: 2023/12/22 11:31:10 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../materia.h"

// Default constructor
Ice::Ice(void)
{
    std::cout << CYAN << "Ice default constructor called" << RESET << std::endl;
    this->_type = "ice";
    return ;
}

// Copy constructor
Ice::Ice(const Ice &other)
{
    std::cout << CYAN << "Ice copy constructor called" << RESET << std::endl;
    this->_type = other._type;
    return ;
}

// Assignment operator overload
Ice &Ice::operator=(const Ice &other)
{
    std::cout << CYAN << "Ice assignment operator called" << RESET << std::endl;
    this->_type = other._type;
    return (*this);
}

// Destructor
Ice::~Ice(void)
{
    std::cout << CYAN << "Ice destructor called" << RESET << std::endl;
    return ;
}

void Ice::Use(ICharacter& target)
{
	std::cout << CYAN << "* shoots an ice bolt at " << target.GetName() << " *" << RESET << std::endl; 
	return ;
}

AMateria* Ice::Clone() const
{
    std::cout << CYAN << "Cloning Ice materia." << RESET << std::endl;
	return (new Ice(*(this)));
}
