/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Character.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/21 15:57:14 by sada-sil          #+#    #+#             */
/*   Updated: 2023/12/21 16:23:18 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../materia.h"

// Default constructor
Character::Character(void)
{
    std::cout << RED << "Character default constructor called" << RESET << std::endl;
    this->_name = "Default";
    for (int i = 0; i <= 3; i++)
        _inventory[i] = NULL;
    return ;
}

Character::Character(std::string name)
{
    std::cout << RED << "Character name constructor called" << RESET << std::endl;
    this->_name = name;
    for (int i = 0; i <= 3; i++)
        _inventory[i] = NULL;
    return ;
}

// Copy constructor
Character::Character(const Character &other)
{
    std::cout << RED << "Character copy constructor called" << RESET << std::endl;
    this->_name = other._name;

    for (int i = 0; i <= 3; i++)
    {
        if (other._inventory[i] != NULL)
            _inventory[i] = other._inventory[i]->Clone();
        else
            _inventory[i] = NULL;
    }
    return ;
}

// Assignment operator overload
Character &Character::operator=(const Character &other)
{
    std::cout << RED << "Character assignment operator called" << RESET << std::endl;
    this->_name = other._name;
    
    for (int i = 0; i <= 3; i++)
    {
        if (_inventory[i] != NULL)
            delete _inventory[i];
        if (other._inventory[i] != NULL)
            _inventory[i] = other._inventory[i]->Clone();
        else
            _inventory[i] = NULL;
    }
    return (*this);
}

// Destructor
Character::~Character(void)
{
    std::cout << RED << "Character destructor called, name was: " << this->GetName() << RESET << std::endl;
    for (int i = 0; i <= 3; i++)
    {
        if (_inventory[i] != NULL)
        {
            delete _inventory[i];
            _inventory[i] = NULL;
        }
    }
    return ;
}

std::string const &Character::GetName(void) const
{
    return this->_name;
}

void	Character::Equip(AMateria *m)
{
	if (m != NULL && m != 0)
	{
		std::cout << RED << this->_name << " equips " << m->GetType() << " materia." << RESET << std::endl;
		for (int i = 0; i <= 3; i++)
		{
			if (_inventory[i] == NULL)
			{
				_inventory[i] = m;
				break;
			}
		}
	}
    return ;
}

void	Character::Unequip(int idx)
{
	std::cout << RED << "Unequiping " << this->GetName() << "'s materia at index " << idx << RESET << std::endl;
    _inventory[idx] = NULL;
    return ;
}

void	Character::Use(int idx, ICharacter &target)
{
	if (_inventory[idx] != NULL)
        _inventory[idx]->Use(target);
    else
        std::cout << RED << "No Materia in this _inventory slot." << RESET << std::endl;
    return ;
}
