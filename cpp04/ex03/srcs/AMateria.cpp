/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   AMateria.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/21 15:57:11 by sada-sil          #+#    #+#             */
/*   Updated: 2023/12/22 11:27:53 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../materia.h"

// Default constructor
AMateria::AMateria(void)
{
    std::cout << WHITE << "AMateria default constructor called" << RESET << std::endl;
	this->_type = "unknown";
    return ;
}

// Typing constructor
AMateria::AMateria(std::string const & type)
{
    std::cout << WHITE << "AMateria typing constructor called" << RESET << std::endl;
	this->_type = type;
    return ;
}

// Copy constructor
AMateria::AMateria(const AMateria &other)
{
    std::cout << WHITE << "AMateria copy constructor called" << RESET << std::endl;
    this->_type = other._type;
    return ;
}

// Assignment operator overload
AMateria &AMateria::operator=(const AMateria &other)
{
    std::cout << WHITE << "AMateria assignment operator called" << RESET << std::endl;
    this->_type = other._type;
    return (*this);
}

// Destructor
AMateria::~AMateria(void)
{
    std::cout << WHITE << "AMateria destructor called, type was : " << this->_type << RESET << std::endl;
    return ;
}

std::string	const &AMateria::GetType(void) const
{
    return this->_type;
}

void AMateria::Use(ICharacter& target)
{
	std::cout << WHITE << this->_type << " type can't be Used." << RESET << std::endl; 
	return ;
}