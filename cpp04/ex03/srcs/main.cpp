/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/22 11:20:42 by sada-sil          #+#    #+#             */
/*   Updated: 2023/12/22 11:22:50 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../materia.h"

int	main(void)
{
	std::cout << std::endl << YELLOW << "Let's create the MateriaSource and 2 Characters." 
		<< RESET << std::endl;
	IMateriaSource* src = new MateriaSource();
	Character* me = new Character("Me");
	ICharacter* bob = new Character("Bob");
	AMateria* tmp1;
	AMateria* tmp2;
	
	std::cout << std::endl << YELLOW << "Now let the MateriaSource object learn the Ice and Cure types."
		<< RESET << std::endl;
	src->LearnMateria(new Ice());
	src->LearnMateria(new Cure());

	std::cout << std::endl << YELLOW << "Let's create the Materias using MateriaSource, and equip them to me."
		<< RESET << std::endl;
	tmp1 = src->CreateMateria("ice");
	me->Equip(tmp1);
	std::cout << std::endl;
	tmp2 = src->CreateMateria("cure");
	me->Equip(tmp2);

	std::cout << std::endl << YELLOW << "If we try creating a non-existant type, it wont create it nor equip it."
		<< RESET << std::endl;
	tmp1 = src->CreateMateria("none");
	me->Equip(tmp1);

	std::cout << std::endl << YELLOW << "Now if we use them, it should print correct messages."
		<< RESET << std::endl;
	me->Use(0, *bob);
	me->Use(1, *bob);

	std::cout << std::endl << YELLOW << "Let's create a copy of the first Character (Me)"
		<< RESET << std::endl;
	ICharacter* meCopy = new Character(*me);

	std::cout << std::endl << YELLOW << "Let's unequip Me's second Materia and delete Me."
		<< RESET << std::endl;
	me->Unequip(1);
	delete me;

	std::cout << std::endl << YELLOW << "We can see that the unequiped Materia hasn't been deleted."
		<< RESET << std::endl;
	std::cout << WHITE << "Unequiped Materia's type: " << tmp2->GetType() << RESET << std::endl;

	std::cout << std::endl << YELLOW << "We must then delete it indivually to avoid leaks."
		<< RESET << std::endl;
	delete tmp2;

	std::cout << std::endl << YELLOW << "We can see that the Me's copy isn't affected by it's original's deletion, since it's a deep copy."
		<< RESET << std::endl;
	meCopy->Use(0, *bob);

	std::cout << std::endl << YELLOW << "Finally, lets delete the MateriaSource and all the Character objects."
		<< RESET << std::endl;
	delete src;
	delete bob;
	delete meCopy;
	std::cout << YELLOW << "We can see that all the equiped Materias were also deleted."
		<< RESET << std::endl << std::endl;
	return 0;
}