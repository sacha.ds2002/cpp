/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Cure.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/21 15:57:16 by sada-sil          #+#    #+#             */
/*   Updated: 2023/12/22 11:31:36 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../materia.h"

// Default constructor
Cure::Cure(void)
{
    std::cout << GREEN << "Cure default constructor called" << RESET << std::endl;
    this->_type = "cure";
    return ;
}

// Copy constructor
Cure::Cure(const Cure &other)
{
    std::cout << GREEN << "Cure copy constructor called" << RESET << std::endl;
    this->_type = other._type;
    return ;
}

// Assignment operator overload
Cure &Cure::operator=(const Cure &other)
{
    std::cout << GREEN << "Cure assignment operator called" << RESET << std::endl;
    this->_type = other._type;
    return (*this);
}

// Destructor
Cure::~Cure(void)
{
    std::cout << GREEN << "Cure destructor called" << RESET << std::endl;
    return ;
}

void Cure::Use(ICharacter& target)
{
	std::cout << GREEN << "* heals " << target.GetName() << "'s wounds *" << RESET << std::endl; 
	return ;
}

AMateria* Cure::Clone() const
{
    std::cout << GREEN << "Cloning Cure materia." << RESET << std::endl;
	return (new Cure(*(this)));
}