/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   MateriaSource.cpp                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/21 15:57:31 by sada-sil          #+#    #+#             */
/*   Updated: 2023/12/22 11:20:26 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../materia.h"

// Default constructor
MateriaSource::MateriaSource(void)
{
    std::cout << MAGENTA << "MateriaSource default constructor called" << RESET << std::endl;
    for (int i = 0; i <= 3; i++)
        _materias[i] = NULL;
    return ;
}

// Copy constructor
MateriaSource::MateriaSource(const MateriaSource &other)
{
    std::cout << MAGENTA << "MateriaSource copy constructor called" << RESET << std::endl;
    for (int i = 0; i <= 3; i++)
    {
        if (other._materias[i] != NULL)
            _materias[i] = other._materias[i]->Clone();
        else
            _materias[i] = NULL;
    }
    return ;
}

// Assignment operator overload
MateriaSource &MateriaSource::operator=(const MateriaSource &other)
{
    std::cout << MAGENTA << "MateriaSource assignment operator called" << RESET << std::endl;
    for (int i = 0; i <= 3; i++)
    {
        if (_materias[i] != NULL)
            delete _materias[i];
        if (other._materias[i] != NULL)
            _materias[i] = other._materias[i]->Clone();
        else
            _materias[i] = NULL;
    }
    return (*this);
}

// Destructor
MateriaSource::~MateriaSource(void)
{
    std::cout << MAGENTA << "MateriaSource destructor called" << RESET <<  std::endl;
    for (int i = 0; i <= 3; i++)
    {
        if (_materias[i] != NULL)
        {
            delete _materias[i];
            _materias[i] = NULL;
        }
    }
    return ;
}

void	MateriaSource::LearnMateria(AMateria *materia)
{
    std::cout << MAGENTA << "LearnMateria called" << RESET << std::endl;
	for (int i = 0; i <= 3; i++)
    {
        if (_materias[i] == NULL)
        {
            _materias[i] = materia;
            break;
        }
    }
    return ;
}

AMateria	*MateriaSource::CreateMateria(std::string const & type)
{
    std::cout << MAGENTA << "CreateMateria called with " << type << RESET << std::endl;
	for (int i = 0; i <= 3; i++)
	{
		if (_materias[i] != NULL
			&& _materias[i]->GetType() == type)
			return _materias[i]->Clone();
	}
	std::cout << MAGENTA << type << " type not found."  << RESET << std::endl;
    return 0;
}
