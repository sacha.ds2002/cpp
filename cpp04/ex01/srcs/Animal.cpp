/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Animal.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/20 09:57:31 by sada-sil          #+#    #+#             */
/*   Updated: 2023/12/21 12:17:56 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../animal.h"

// Default constructor
Animal::Animal(void)
{
    std::cout << WHITE << "Animal default constructor called" << RESET << std::endl;
    return ;
}

// Copy constructor
Animal::Animal(const Animal &other)
{
    std::cout << WHITE << "Animal copy constructor called" << RESET << std::endl;
    _type = other._type;
    return ;
}

// Assignment operator overload
Animal &Animal::operator=(const Animal &other)
{
    std::cout << WHITE << "Animal assignment operator called" << RESET << std::endl;
    _type = other._type;
    return (*this);
}

// Destructor
Animal::~Animal(void)
{
    std::cout << WHITE << "Animal destructor called" << RESET << std::endl;
    return ;
}

std::string Animal::GetType() const
{
	if (_type.empty())
		return ("unknown");
	return (_type);	
}

void Animal::MakeNoise() const
{
	std::cout << WHITE << "..." << RESET << std::endl;
}
