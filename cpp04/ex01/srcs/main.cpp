/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/20 09:58:14 by sada-sil          #+#    #+#             */
/*   Updated: 2023/12/21 15:11:21 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../animal.h"

int	main(void)
{
	int	len = 6;
	Animal *animals[len];

	std::cout << std::endl << MAGENTA << "Let's create the cats and dogs using polymorphism." << RESET << std::endl;
	for (int i = 0; i < len; i++)
	{
		if (i < len / 2)
			animals[i] = new Dog();
		else
			animals[i] = new Cat();
	}

	std::cout << std::endl << MAGENTA << "We can then use their MakeNoise functions." << RESET << std::endl;
	for (int i = 0; i < len; i++)
	{
		std::cout << WHITE << "Type: " << animals[i]->GetType()  << " | Noise: "<< RESET;
		animals[i]->MakeNoise();
	}
	
	std::cout << std::endl << MAGENTA << "Now let's delete them." << RESET << std::endl;
	for (int i = 0; i < len; i++)
		delete animals[i];

	std::cout << std::endl << MAGENTA << "Copying a cat or a dog also copies all their members." << RESET << std::endl;
	Cat *cat1 = new Cat();
	Cat *cat2 = new Cat(*cat1);

	std::cout << std::endl;
	delete cat1;
	delete cat2;
	return 0;
}