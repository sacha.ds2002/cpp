/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Brain.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/20 11:19:58 by sada-sil          #+#    #+#             */
/*   Updated: 2023/12/21 14:57:05 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../animal.h"

// Default constructor
Brain::Brain(void)
{
    std::cout << BLUE << "Brain's default constructor called" << RESET << std::endl;
    return ;
}

// Copy constructor
Brain::Brain(const Brain &other)
{
    std::cout << BLUE << "Brain's copy constructor called" << RESET << std::endl;
    for (int i = 0; i < 100; i++)
	{
		this->ideas[i].clear();
		this->ideas[i] = other.ideas[i];
	}
    return ;
}

// Assignment operator overload
Brain &Brain::operator=(const Brain &other)
{
    std::cout << BLUE << "Brain's assignment operator called" << RESET << std::endl;
    for (int i = 0; i < 100; i++)
	{
		this->ideas[i].clear();
		this->ideas[i] = other.ideas[i];
	}
    return (*this);
}

// Destructor
Brain::~Brain(void)
{
    std::cout << BLUE << "Brain's destructor called" << RESET << std::endl;
    return ;
}

