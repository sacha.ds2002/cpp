/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Dog.cpp                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/20 10:02:58 by sada-sil          #+#    #+#             */
/*   Updated: 2023/12/21 13:46:35 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../animal.h"

// Default constructor
Dog::Dog(void)
{
    std::cout << RED << "Dog default constructor called" << RESET << std::endl;
	_type = "Dog";
	_brain = new Brain();
    return ;
}

// Copy constructor
Dog::Dog(const Dog &other)
{
    std::cout << RED << "Dog copy constructor called" << RESET << std::endl;
    this->_type = other._type;
	this->_brain = new Brain(*(other._brain));
    return ;
}

// Assignment operator overload
Dog &Dog::operator=(const Dog &other)
{
    std::cout << RED << "Dog assignment operator called" << RESET << std::endl;
    this->_type = other._type;
	delete this->_brain;
	this->_brain = new Brain(*(other._brain));
    return (*this);
}

// Destructor
Dog::~Dog(void)
{
    std::cout << RED << "Dog destructor called" << RESET << std::endl;
	delete this->_brain;
    return ;
}

void Dog::MakeNoise() const
{
	std::cout << RED << "Waf waf" << RESET << std::endl;
}
