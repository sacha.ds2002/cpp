/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Cat.hpp                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/20 10:03:07 by sada-sil          #+#    #+#             */
/*   Updated: 2023/12/21 15:12:31 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CAT_HPP
# define CAT_HPP
# include "../animal.h"

class Cat : public Animal
{
    public:
        Cat(void);
        Cat(Cat& other);
        Cat &operator=(const Cat &other);
        ~Cat();

		virtual void MakeNoise() const;
	
	private:
		Brain *_brain;
};

#endif

