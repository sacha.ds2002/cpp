/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Cat.cpp                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/20 10:03:27 by sada-sil          #+#    #+#             */
/*   Updated: 2023/12/21 15:19:40 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../animal.h"

// Default constructor
Cat::Cat(void)
{
    std::cout << GREEN << "Cat default constructor called" << std::endl;
	this->_type = "Cat";
	this->_brain = new Brain();
    return ;
}

// Copy constructor
Cat::Cat(const Cat &other)
{
    std::cout << GREEN << "Cat copy constructor called" << std::endl;
    this->_type = other._type;
	this->_brain = new Brain(*(other._brain));
    return ;
}

// Assignment operator overload
Cat &Cat::operator=(const Cat &other)
{
    std::cout << GREEN << "Cat assignment operator called" << std::endl;
    this->_type = other._type;
	if (this->_brain != NULL)
		delete this->_brain;
	this->_brain = new Brain(*(other._brain));
    return (*this);
}

// Destructor
Cat::~Cat(void)
{
    std::cout << GREEN << "Cat destructor called" << std::endl;
	delete this->_brain;
    return ;
}

void Cat::MakeNoise() const
{
	std::cout << GREEN << "Meow" << RESET << std::endl;
}
