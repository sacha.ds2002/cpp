/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Animal.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/20 09:57:31 by sada-sil          #+#    #+#             */
/*   Updated: 2023/12/21 15:22:40 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../animal.h"

// Default constructor
AAnimal::AAnimal(void)
{
    std::cout << WHITE << "AAnimal default constructor called" << RESET << std::endl;
    return ;
}

// Copy constructor
AAnimal::AAnimal(const AAnimal &other)
{
    std::cout << WHITE << "AAnimal copy constructor called" << RESET << std::endl;
    _type = other._type;
    return ;
}

// Assignment operator overload
AAnimal &AAnimal::operator=(const AAnimal &other)
{
    std::cout << WHITE << "AAnimal assignment operator called" << RESET << std::endl;
    _type = other._type;
    return (*this);
}

// Destructor
AAnimal::~AAnimal(void)
{
    std::cout << WHITE << "AAnimal destructor called" << RESET << std::endl;
    return ;
}

std::string AAnimal::GetType() const
{
	if (_type.empty())
		return ("unknown");
	return (_type);	
}
