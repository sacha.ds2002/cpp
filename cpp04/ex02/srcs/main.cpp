/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/20 09:58:14 by sada-sil          #+#    #+#             */
/*   Updated: 2023/12/21 15:17:34 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../animal.h"

int	main(void)
{
	int	len = 6;
	AAnimal *animals[len];

	std::cout << std::endl << MAGENTA << "Let's create the cats and dogs using polymorphism." << RESET << std::endl;
	for (int i = 0; i < len; i++)
	{
		if (i < len / 2)
			animals[i] = new Dog();
		else
			animals[i] = new Cat();
	}

	std::cout << std::endl << MAGENTA << "We can then use their MakeNoise functions." << RESET << std::endl;
	for (int i = 0; i < len; i++)
	{
		std::cout << WHITE << "Type: " << animals[i]->GetType()  << " | Noise: "<< RESET;
		animals[i]->MakeNoise();
	}
	
	std::cout << std::endl << MAGENTA << "Now let's delete them." << RESET << std::endl;
	for (int i = 0; i < len; i++)
	{
		delete animals[i];
	}

	std::cout << std::endl << MAGENTA << "But now we can't initialize an Animal directly, because it's an abstract class." << RESET << std::endl;
	//AAnimal *animal = new AAnimal(); --> error
	
	return 0;
}