/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Animal.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/20 09:56:25 by sada-sil          #+#    #+#             */
/*   Updated: 2023/12/21 15:18:52 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ANIMAL_HPP
# define ANIMAL_HPP
# include "../animal.h"

class AAnimal
{
    public:
        AAnimal(void);
        AAnimal(const AAnimal& other);
        AAnimal &operator=(const AAnimal &other);
        virtual ~AAnimal();

		std::string GetType() const;

		virtual void MakeNoise() const = 0;

	protected:
		std::string _type;
};

#endif

