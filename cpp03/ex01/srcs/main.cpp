/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/14 11:36:17 by sada-sil          #+#    #+#             */
/*   Updated: 2023/12/19 14:19:41 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../clapTrap.h"

int	main(void)
{
	ClapTrap clap("Clapi");
	ScavTrap scav("Scavi");

	std::cout << std::endl << WHITE << "Here is " << clap.GetName() 
		<< " (claptrap) stats: " << std::endl << "HP: " << clap.GetHP()
		<< std::endl << "Energy: " << clap.GetEP()
		<< std::endl << "Attack: " << clap.GetAtk()
		<< RESET << std::endl;

	std::cout << std::endl << WHITE << "Here is " << scav.GetName() 
		<< " (scavtrap) stats: " << std::endl << "HP: " << scav.GetHP()
		<< std::endl << "Energy: " << scav.GetEP()
		<< std::endl << "Attack: " << scav.GetAtk()
		<< RESET << std::endl;

	std::cout << std::endl << WHITE << clap.GetName() << " attack " 
		<< scav.GetName()
		<< RESET << std::endl;
	clap.Attack(scav.GetName());
	scav.TakeDamage(clap.GetAtk());

	std::cout << std::endl << WHITE << scav.GetName() << " attack " 
		<< clap.GetName()
		<< RESET << std::endl;
	scav.Attack(clap.GetName());
	clap.TakeDamage(scav.GetAtk());

	std::cout << std::endl << WHITE << "Let's " << RED
		<< "kill " << WHITE << scav.GetName()
		<< RESET << std::endl;
	clap.SetHP(10);
	clap.SetAtk(100);
	clap.Attack(scav.GetName());
	scav.TakeDamage(clap.GetAtk());

	std::cout << std::endl << WHITE << "Now that " << scav.GetName() 
		<< " is dead, they can't do anything."
		<< RESET << std::endl;
	scav.Attack(clap.GetName());
	scav.BeRepaired(5);
	clap.Attack(scav.GetName());
	scav.TakeDamage(clap.GetAtk());
	
	std::cout << std::endl << WHITE << "Scavtraps can also heal if they have at least one HP."
		<< RESET << std::endl;
	scav.SetHP(1);
	scav.BeRepaired(200);
	std::cout << scav.GetName() << "'s HP: " << scav.GetHP() << std::endl;
	
	std::cout << std::endl << WHITE << "Scavtraps special ability:"
		<< RESET << std::endl;
	scav.GuardGate();
	
	std::cout << std::endl << WHITE << "They can escape this state by Gate Keeping a second time:"
		<< RESET << std::endl;
	scav.GuardGate();
	
	std::cout << std::endl;
}