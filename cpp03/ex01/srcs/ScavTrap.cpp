/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ScavTrap.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/14 14:54:15 by sada-sil          #+#    #+#             */
/*   Updated: 2023/12/19 14:42:55 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../clapTrap.h"

// Default constructor
ScavTrap::ScavTrap(void)
{
    std::cout << CYAN << "ScavTrap default constructor called" << RESET << std::endl;
	_name = "blank";
	_hp = 100;
	_maxHP = _hp;
	_ep = 50;
	_atk = 20;
	isGateKeeping = false;
    return ;
}

// Name constructor
ScavTrap::ScavTrap(std::string name)
{
    std::cout << CYAN << "ScavTrap name constructor called" << RESET << std::endl;
	if (!name.empty())
		_name = name;
	else
	{
		std::cout << "Name is empty." << std::endl;
		_name = "Blank";
	}
	_hp = 100;
	_maxHP = _hp;
	_ep = 50;
	_atk = 20;
	isGateKeeping = false;
    return ;
}

// Copy constructor
ScavTrap::ScavTrap(const ScavTrap &other)
{
    std::cout << CYAN << "ScavTrap copy constructor called" << RESET << std::endl;
	this->_name = other._name;
    this->_hp = other._hp;
	this->_maxHP = other._maxHP;
	this->_ep = other._ep;
	this->_atk = other._atk;
    return ;
}

// Assignment operator overload
ScavTrap &ScavTrap::operator=(const ScavTrap &other)
{
    std::cout << CYAN << "ScavTrap assignment operator called" << RESET << std::endl;
    this->_name = other._name;
    this->_hp = other._hp;
	this->_maxHP = other._maxHP;
	this->_ep = other._ep;
	this->_atk = other._atk;
    return (*this);
}

// Destructor
ScavTrap::~ScavTrap(void)
{
    std::cout << CYAN << "ScavTrap destructor called" << RESET << std::endl;
    return ;
}

void	ScavTrap::Attack(const std::string &target)
{
	if (this->_hp > 0 && this->_UseEP(1))
	{
		std::cout << RED << "ScavTrap " << this->_name << " attacks " 
			<< target << ", causing " << this->_atk << " points of damage !"
			<< RESET << std::endl;
	}
	else if (this->_hp == 0)
		std::cout << YELLOW << "ScavTrap " << this->_name 
			<< " tried to attack, but they are dead."
			<< RESET << std::endl;	
}

void	ScavTrap::GuardGate(void)
{
	if (!isGateKeeping)
	{
		isGateKeeping = true;
		std::cout << MAGENTA << "ScavTrap " << this->_name 
			<< " is now Gate Keeping."
			<< RESET << std::endl;
	}
	else
	{
		isGateKeeping = false;
		std::cout << MAGENTA << "ScavTrap " << this->_name 
			<< " is no longer Gate Keeping."
			<< RESET << std::endl;
	}
}
