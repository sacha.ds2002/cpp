/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   FragTrap.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/14 14:54:15 by sada-sil          #+#    #+#             */
/*   Updated: 2023/12/19 14:36:16 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../clapTrap.h"

// Default constructor
FragTrap::FragTrap(void)
{
    std::cout << CYAN << "FragTrap default constructor called" << RESET << std::endl;
	_name = "blank";
	_hp = 100;
	_maxHP = _hp;
	_ep = 100;
	_atk = 30;
    return ;
}

// Name constructor
FragTrap::FragTrap(std::string name)
{
    std::cout << CYAN << "FragTrap name constructor called" << RESET << std::endl;
	if (!name.empty())
		_name = name;
	else
	{
		std::cout << CYAN << "Name is empty." << RESET << std::endl;
		_name = "Blank";
	}
	_hp = 100;
	_maxHP = _hp;
	_ep = 100;
	_atk = 30;
    return ;
}

// Copy constructor
FragTrap::FragTrap(const FragTrap &other)
{
    std::cout << CYAN << "FragTrap copy constructor called" << RESET << std::endl;
	this->_name = other._name;
    this->_hp = other._hp;
	this->_maxHP = other._maxHP;
	this->_ep = other._ep;
	this->_atk = other._atk;
    return ;
}

// Assignment operator overload
FragTrap &FragTrap::operator=(const FragTrap &other)
{
    std::cout << CYAN << "ScavTrap assignment operator called" << RESET << std::endl;
    this->_name = other._name;
    this->_hp = other._hp;
	this->_maxHP = other._maxHP;
	this->_ep = other._ep;
	this->_atk = other._atk;
    return (*this);
}

// Destructor
FragTrap::~FragTrap(void)
{
    std::cout << CYAN << "FragTrap destructor called" << RESET << std::endl;
    return ;
}

void	FragTrap::Attack(const std::string &target)
{
	if (this->_hp > 0 && this->_UseEP(1))
	{
		std::cout << RED << "FragTrap " << this->_name << " attacks " 
			<< target << ", causing " << this->_atk << " points of damage !"
			<< RESET << std::endl;
	}
	else if (this->_hp == 0)
		std::cout << YELLOW << "FragTrap " << this->_name 
			<< " tried to attack, but they are dead."
			<< RESET << std::endl;	
}

void	FragTrap::HighFivesGuys(void)
{
	std::cout << MAGENTA << "FragTrap " << this->_name 
			<< " wants to High Five !"
			<< RESET << std::endl;
}
