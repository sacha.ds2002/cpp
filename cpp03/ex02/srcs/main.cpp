/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/14 11:36:17 by sada-sil          #+#    #+#             */
/*   Updated: 2023/12/19 14:40:13 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../clapTrap.h"

int	main(void)
{
	ClapTrap clap("Clapi");
	ScavTrap scav("Scavi");
	FragTrap frag("Fraggi");

	std::cout << std::endl << WHITE << "Here is " << clap.GetName() 
		<< " (claptrap) stats: " << std::endl << "HP: " << clap.GetHP()
		<< std::endl << "Energy: " << clap.GetEP()
		<< std::endl << "Attack: " << clap.GetAtk()
		<< RESET << std::endl;

	std::cout << std::endl << WHITE << "Here is " << scav.GetName() 
		<< " (scavtrap) stats: " << std::endl << "HP: " << scav.GetHP()
		<< std::endl << "Energy: " << scav.GetEP()
		<< std::endl << "Attack: " << scav.GetAtk()
		<< RESET << std::endl;

		std::cout << std::endl << WHITE << "Here is " << frag.GetName() 
		<< " (fragtrap) stats: " << std::endl << "HP: " << frag.GetHP()
		<< std::endl << "Energy: " << frag.GetEP()
		<< std::endl << "Attack: " << frag.GetAtk()
		<< RESET << std::endl;

	std::cout << std::endl << WHITE << scav.GetName() << " attack " 
		<< frag.GetName()
		<< RESET << std::endl;
	scav.Attack(frag.GetName());
	frag.TakeDamage(scav.GetAtk());

	std::cout << std::endl << WHITE << "Let's " << RED
		<< "kill " << WHITE << frag.GetName()
		<< RESET << std::endl;
	scav.SetAtk(100);
	scav.Attack(frag.GetName());
	frag.TakeDamage(scav.GetAtk());


	std::cout << std::endl << WHITE << "Now that " << frag.GetName() 
		<< " is dead, they can't do anything."
		<< RESET << std::endl;
	frag.Attack(clap.GetName());
	frag.BeRepaired(5);
	scav.Attack(frag.GetName());
	frag.TakeDamage(scav.GetAtk());
	
	std::cout << std::endl << WHITE << "FragTraps can also heal if they have at least one HP."
		<< RESET << std::endl;
	frag.SetHP(1);
	frag.BeRepaired(200);
	std::cout << frag.GetName() << "'s HP: " << frag.GetHP() << std::endl;
	
	std::cout << std::endl << WHITE << "FragTraps special ability:"
		<< RESET << std::endl;
	frag.HighFivesGuys();
	
	std::cout << std::endl;
}