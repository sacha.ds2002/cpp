/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ClapTrap.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/14 11:36:24 by sada-sil          #+#    #+#             */
/*   Updated: 2023/12/19 14:11:01 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CLAPTRAP_HPP
# define CLAPTRAP_HPP
# include "../clapTrap.h"

class ClapTrap
{
    public:
        ClapTrap(void);
		ClapTrap(std::string name);
        ClapTrap(const ClapTrap& other);
        ClapTrap &operator=(const ClapTrap &other);
        ~ClapTrap();

		void	Attack(const std::string &target);
		void	TakeDamage(unsigned int amount);
		void	BeRepaired(unsigned int amount);

		std::string GetName(void) const;
		void		SetName(std::string name);
		int			GetHP(void) const;
		void		SetHP(int hp);
		int			GetEP(void) const;
		void		SetEP(int hp);
		int			GetAtk(void) const;
		void		SetAtk(int hp);

	private:
		std::string  _name;
		unsigned int _hp;
		unsigned int _ep;
		unsigned int _atk;

		unsigned int _maxHP;
		
		bool		 _UseEP(int amount);
};

#endif

