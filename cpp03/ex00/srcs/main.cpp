/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/14 11:36:17 by sada-sil          #+#    #+#             */
/*   Updated: 2023/12/19 14:21:02 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../clapTrap.h"

int	main(void)
{
	ClapTrap Alan("Alan");
	ClapTrap Amy("Amy");

	std::cout << std::endl << WHITE << "Alan attack Amy:" << RESET << std::endl;
	Alan.Attack(Amy.GetName());
	Amy.TakeDamage(Alan.GetAtk());
	Amy.BeRepaired(Amy.GetAtk());

	std::cout << std::endl << WHITE 
		<< "Let's raise their attack to two." << RESET << std::endl;
	Alan.SetAtk(2);
	Amy.SetAtk(2);
	Alan.Attack(Amy.GetName());
	Amy.TakeDamage(Alan.GetAtk());
	Amy.BeRepaired(Amy.GetAtk());

	std::cout << std::endl << WHITE << "Let's " << RED
		<< "kill " << WHITE << Amy.GetName()
		<< RESET << std::endl;
	Alan.SetAtk(10);
	Alan.Attack(Amy.GetName());
	Amy.TakeDamage(Alan.GetAtk());

	std::cout << std::endl << WHITE << "Now that " << Amy.GetName() 
		<< " is dead, they can't do anything."
		<< RESET << std::endl;
	Amy.Attack(Alan.GetName());
	Amy.BeRepaired(5);
	Alan.Attack(Amy.GetName());
	Amy.TakeDamage(Alan.GetAtk());
	
	std::cout << std::endl << WHITE << "Let's set " << Amy.GetName() 
		<< "'s HP back to 10 and " << Alan.GetName()
		<< "'s attack to 1."
		<< RESET << std::endl;
	Amy.SetHP(10);
	Alan.SetAtk(1);
	
	std::cout << std::endl << WHITE << Alan.GetName() << " has " 
		<< Alan.GetEP() << " energy points left."
		<< RESET << std::endl;
	while (Alan.GetEP() > 0)
	{
		Alan.Attack(Amy.GetName());
		Amy.TakeDamage(Alan.GetAtk());
	}
	Alan.Attack(Amy.GetName());
	Amy.BeRepaired(10);
	
	std::cout << std::endl;
}