/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ClapTrap.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/14 11:36:20 by sada-sil          #+#    #+#             */
/*   Updated: 2023/12/19 14:20:49 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../clapTrap.h"

// Default constructor
ClapTrap::ClapTrap(void)
{
    std::cout << BLUE << "ClapTrap default constructor called" << RESET << std::endl;
	_name = "Blank";
	_hp = 10;
	_maxHP = _hp;
	_ep = 10;
	_atk = 0;
    return ;
}

// Name constructor
ClapTrap::ClapTrap(std::string name)
{
    std::cout << BLUE << "ClapTrap name constructor called" << RESET << std::endl;
	if (!name.empty())
		_name = name;
	else
	{
		std::cout << BLUE << "Name is empty." << RESET << std::endl;
		_name = "Blank";
	}
	_hp = 10;
	_maxHP = _hp;
	_ep = 10;
	_atk = 0;
    return ;
}

// Copy constructor
ClapTrap::ClapTrap(const ClapTrap &other) : _name(other._name), _hp(other._hp), _maxHP(other._maxHP),
	_ep(other._ep), _atk(other._atk)
{
    std::cout << BLUE << "ClapTrap copy constructor called" << RESET << std::endl;
    return ;
}

// Assignment operator overload
ClapTrap &ClapTrap::operator=(const ClapTrap &other)
{
    std::cout << BLUE << "ClapTrap assignment operator called" << RESET << std::endl;
    this->_name = other._name;
    this->_hp = other._hp;
	this->_maxHP = other._maxHP;
	this->_ep = other._ep;
	this->_atk = other._atk;
    return (*this);
}

// Destructor
ClapTrap::~ClapTrap(void)
{
    std::cout << BLUE << "ClapTrap destructor called" << RESET << std::endl;
    return ;
}

std::string ClapTrap::GetName(void) const
{
	return (this->_name);
}

void ClapTrap::SetName(std::string name)
{
	if (!name.empty())
	{
		std::cout << MAGENTA << "Set " << _name << "'s name to " << name
			<< RESET << std::endl;
		this->_name = name;		
	}
}

int ClapTrap::GetHP(void) const
{
	return (this->_hp);
}

void ClapTrap::SetHP(int hp)
{
	std::cout << MAGENTA << "Set " << _name << "'s HP to " << hp
		<< RESET << std::endl;
	this->_hp = hp;
}

int ClapTrap::GetEP(void) const
{
	return (this->_ep);
}

void ClapTrap::SetEP(int ep)
{
	std::cout << MAGENTA << "Set " << _name << "'s energy to " << ep
		<< RESET << std::endl;
	this->_ep = ep;
}

bool ClapTrap::_UseEP(int amount)
{
	if (_ep == 0)
	{
		std::cout << YELLOW << "ClapTrap " << this->_name 
			<< " has no Energy Point left."
			<< RESET << std::endl;
		return (false);
	}
	if (amount > this->_ep)
		_ep = 0;
	else
		_ep -= amount;
	return (true);
}

int ClapTrap::GetAtk(void) const
{
	return (this->_atk);
}

void ClapTrap::SetAtk(int atk)
{
	std::cout << MAGENTA << "Set " << _name << "'s attack to " << atk
		<< RESET << std::endl;
	this->_atk = atk;
}

void	ClapTrap::Attack(const std::string &target)
{
	if (this->_hp > 0 && this->_UseEP(1))
	{
		std::cout << RED << "ClapTrap " << this->_name << " attacks " 
			<< target << ", causing " << this->_atk << " points of damage !"
			<< RESET << std::endl;
	}
	else if (this->_hp == 0)
		std::cout << YELLOW << "ClapTrap " << this->_name 
			<< " tried to attack, but they are dead."
			<< RESET << std::endl;
}

void	ClapTrap::TakeDamage(unsigned int amount)
{
	if (this->_hp == 0)
	{
		std::cout << YELLOW << "ClapTrap " << this->_name 
			<< " is already dead."
			<< RESET << std::endl;
		return ;
	}
	if (amount > this->_hp)
		_hp = 0;
	else
		_hp -= amount;
	std::cout << CYAN << "ClapTrap " << this->_name 
		<< " took " << amount << " damage!"
		<< RESET << std::endl;
	if (_hp == 0)
		std::cout << YELLOW << "ClapTrap " << this->_name 
			<< " is dead!"
			<< RESET << std::endl;
}

void	ClapTrap::BeRepaired(unsigned int amount)
{
	int	tmp;
	
	if (_hp == _maxHP)
	{
		std::cout << YELLOW << "ClapTrap " << this->_name 
			<< " tried to heal, but they are fully healed."
			<< RESET << std::endl;
		return;
	}
	if (this->_hp == 0)
	{
		std::cout << YELLOW << "ClapTrap " << this->_name 
			<< " tried to heal, but they are dead."
			<< RESET << std::endl;
		return ;
	}
	if (this->_UseEP(1))
	{
		tmp = _hp;
		_hp += amount;
		if (_hp > _maxHP)
			_hp = _maxHP;
		std::cout << GREEN << "ClapTrap " << this->_name 
			<< " has been repaired for " << (_hp - tmp) << " HP!"
			<< RESET << std::endl;
	}
}
