/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/23 14:01:14 by sada-sil          #+#    #+#             */
/*   Updated: 2023/11/23 14:09:48 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include <string>

int	main(void)
{
	// Create string and vars
	std::string string = "HI THIS IS BRAIN";
	std::string *stringPTR = &string;
	std::string &stringREF = string;
	
	// Print addresses
	std::cout << std::endl << "Variable address:\t" << &string << std::endl;
	std::cout << "Pointer address: \t" << stringPTR << std::endl;
	std::cout << "Reference address: \t" << &stringREF << std::endl << std::endl;

	// Print values
	std::cout << "Variable value:\t\t" << string << std::endl;
	std::cout << "Pointer value:\t\t" << *stringPTR << std::endl;
	std::cout << "Reference value:\t" << stringREF << std::endl;	
}