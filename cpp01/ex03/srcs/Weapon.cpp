/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Weapon.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/23 14:15:46 by sada-sil          #+#    #+#             */
/*   Updated: 2023/11/23 15:04:52 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

# include "../weapon.h"

// Default constructor
Weapon::Weapon(void)
{
    std::cout << "Default constructor called" << std::endl;
    return ;
}

// Advanced constructor
Weapon::Weapon(std::string type) : _type(type)
{
    std::cout << "Weapon created" << std::endl;
    return ;
}

// Destructor
Weapon::~Weapon(void)
{
    std::cout << "Weapon destroyed" << std::endl;
    return ;
}

std::string	&Weapon::GetType(void)
{
	std::string &ref = _type;
	return ref;
}

void	Weapon::SetType(std::string newType)
{
	if (!newType.empty())
		_type = newType;
}
