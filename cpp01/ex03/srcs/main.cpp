/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/23 14:12:40 by sada-sil          #+#    #+#             */
/*   Updated: 2023/11/23 15:31:02 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../weapon.h"

int main(void)
{
	{
		std::cout << std::endl << YELLOW << "First, let's create a weapon with type 'crude spiked club', equip it to a HumanA and make them attack with it." << RESET << std::endl;
		Weapon club = Weapon("crude spiked club");
		HumanA bob("Bob", club);
		bob.Attack();
		std::cout << YELLOW << "Let's change the weapon type, directly from the weapon and not from the HumanA" << RESET << std::endl;
		club.SetType("some other type of club");
		bob.Attack();
		std::cout << YELLOW << "We can see that the weapon type changed even for the human, so this means that the human has the reference to the weapon." << RESET << std::endl;
	}
	{
		std::cout << std::endl << YELLOW << "Let's do the same with another weapon with type 'crude spiked club', then create a HumanB and equip the weapon afterwards." << RESET << std::endl;
		Weapon club = Weapon("crude spiked club");
		HumanB jim("Jim");
		jim.Attack();
		jim.SetWeapon(club);
		jim.Attack();
		club.SetType("some other type of club");
		jim.Attack();
		std::cout << YELLOW << "As we can see, the HumanB can have no weapon, if he tries to attack without a weapon, it shows a special message." << RESET << std::endl << std::endl;
	}
	return 0;
}

//std::cout << CYAN << "Weapon's address in main: " << &club << std::endl;