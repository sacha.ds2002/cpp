/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   HumanB.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/23 14:15:34 by sada-sil          #+#    #+#             */
/*   Updated: 2023/11/23 15:31:10 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../weapon.h"

// Constructor
HumanB::HumanB(std::string name)
{
	this->SetName(name);
	this->_weapon = NULL;
	std::cout << "HumanA created" << std::endl;
    return ;
}

// Destructor
HumanB::~HumanB(void)
{
    std::cout << "HumanB destroyed" << std::endl;
    return ;
}

void	HumanB::Attack(void) const
{
	if (this->_weapon != NULL)
	{
		std::cout << RED << this->_name << WHITE << " attacks with their "
			<< BLUE << this->GetWeaponType() << RESET << std::endl;
	}
	else
	{
		std::cout << RED << this->_name << WHITE << " doesn't have any weapon equiped. "
			<< RESET << std::endl;
	}
}

std::string	HumanB::GetName(void) const
{
	return _name;
}

void	HumanB::SetName(std::string newName)
{
	if (!newName.empty())
		_name = newName;
}

std::string	HumanB::GetWeaponType(void) const
{
	if (this->_weapon != NULL)
		return (*_weapon).GetType();
	else
		return "non-existent";
}

void	HumanB::SetWeapon(Weapon &weapon)
{
	// std::cout << CYAN << "Weapon's address in HumanB: " << &weapon << RESET << std::endl;
	_weapon = &weapon;
}
