/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   HumanA.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/23 14:13:14 by sada-sil          #+#    #+#             */
/*   Updated: 2023/11/23 15:30:33 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../weapon.h"

// Constructor
HumanA::HumanA(std::string name, Weapon &weapon) : _weapon(weapon)
{
	this->SetName(name);
	std::cout << "HumanA created" << std::endl;
    return ;
}

// Destructor
HumanA::~HumanA(void)
{
    std::cout << "HumanA destroyed" << std::endl;
    return ;
}

void	HumanA::Attack(void) const
{
	std::cout << RED << this->_name << WHITE << " attacks with their "
		<< BLUE << this->GetWeaponType() << RESET << std::endl;
}

std::string	HumanA::GetName(void) const
{
	return _name;
}

void	HumanA::SetName(std::string newName)
{
	if (!newName.empty())
		_name = newName;
}

std::string	HumanA::GetWeaponType(void) const
{
	return _weapon.GetType();
}
