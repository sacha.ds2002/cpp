/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   HumanB.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/23 14:13:34 by sada-sil          #+#    #+#             */
/*   Updated: 2023/11/23 15:29:58 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef HUMANB_HPP
# define HUMANB_HPP
# include "../weapon.h"

class HumanB
{
    public:
		HumanB(std::string name);
		~HumanB();
		
		void	Attack(void) const;
		
		std::string	GetName(void) const;
		void	SetName(std::string newName);
		
		std::string	GetWeaponType(void) const;
		void		SetWeapon(Weapon &weapon);
	
	private:
		std::string _name;
		Weapon *_weapon;
};

#endif

