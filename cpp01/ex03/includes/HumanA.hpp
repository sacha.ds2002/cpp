/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   HumanA.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/23 14:13:47 by sada-sil          #+#    #+#             */
/*   Updated: 2023/11/23 15:20:58 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef HUMANA_HPP
# define HUMANA_HPP
# include "../weapon.h"

class HumanA
{
    public:
		HumanA(std::string name, Weapon &weapon);
        ~HumanA();
		void	Attack(void) const;

		std::string	GetName(void) const;
		void	SetName(std::string newName);
		
		std::string	GetWeaponType(void) const;
		void		SetWeapon(Weapon type);

	private:
		std::string _name;
		Weapon &_weapon;
};

#endif

