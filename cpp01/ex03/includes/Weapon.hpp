/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Weapon.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/23 14:13:53 by sada-sil          #+#    #+#             */
/*   Updated: 2023/11/23 14:44:22 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef WEAPON_HPP
# define WEAPON_HPP
# include "../weapon.h"

class Weapon
{
    public:
        Weapon(void);
		Weapon(std::string type);
        ~Weapon();
		std::string &GetType(void);
		void SetType(std::string newType);

	private:
		std::string _type;
};

#endif

