/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Harl.hpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/24 13:52:32 by sada-sil          #+#    #+#             */
/*   Updated: 2023/11/28 10:20:18 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef HARL_HPP
# define HARL_HPP
# include "../harl.h"

class Harl
{
    public:
        Harl(void);
        ~Harl();
		void	Complain(std::string level);
	
	private:
		void	_Debug(void);
		void	_Info(void);
		void	_Warning(void);
		void	_Error(void);
};

#endif

