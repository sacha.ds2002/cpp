/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/28 09:47:28 by sada-sil          #+#    #+#             */
/*   Updated: 2023/11/28 10:30:21 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../harl.h"

int	main(void)
{
	Harl harl;
	
	std::cout << MAGENTA << "--- Default tests ---" << RESET << std::endl;
	harl.Complain("DEBUG");
	harl.Complain("INFO");
	harl.Complain("WARNING");
	harl.Complain("ERROR");

	std::cout << MAGENTA << "--- Changed order tests ---" << RESET << std::endl;
	harl.Complain("INFO");
	harl.Complain("DEBUG");
	harl.Complain("ERROR");

	std::cout << MAGENTA << "--- Error tests (shouldn't print anything) ---" << RESET << std::endl << std::endl;
	harl.Complain("");
	harl.Complain("Qeiqwneqn");

	return 0;
}