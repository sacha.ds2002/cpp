/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Harl.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/24 13:51:23 by sada-sil          #+#    #+#             */
/*   Updated: 2023/11/28 10:30:17 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../harl.h"

// Default constructor
Harl::Harl(void)
{
    std::cout << "Harl default constructor called" << std::endl << std::endl;
    return ;
}

// Destructor
Harl::~Harl(void)
{
    std::cout << "Harl destructor called" << std::endl;
    return ;
}

void	Harl::Complain(std::string level)
{
	//Create an array containing the levels names, and a function references pointer.
	std::string	levels[4] = {"DEBUG", "INFO", "WARNING", "ERROR"};
	void		(Harl::*func[4])(void);

	//Assign a function's reference to each for each levels, from DEBUG to ERROR
	func[0] = &Harl::_Debug;
	func[1] = &Harl::_Info;
	func[2] = &Harl::_Warning;
	func[3] = &Harl::_Error;

	//Check with a for loop if the given string corresponds to one of the level, if so, call the function using its reference
	for (size_t i = 0; i < levels->length(); i++)
	{
		if (level == levels[i])
		{	
			(this->*func[i])();
			return ;
		}
	}
}

void	Harl::_Debug(void)
{
	std::cout << WHITE << "I love having extra bacon for my "
		<< "7XL-double-cheese-triple-pickle-special- ketchup burger. I really do!"
		<< RESET << std::endl << std::endl;
}

void	Harl::_Info(void)
{
	std::cout << GREEN << "I cannot believe adding extra bacon costs more money. "
		<< "You didn't put enough bacon in my burger! " << std::endl
		<< "If you did, I wouldn't be asking for more!" << RESET << std::endl << std::endl;
}

void	Harl::_Warning(void)
{
	std::cout << YELLOW << "I think I deserve to have some extra bacon for free. " << std::endl
		<< "I've been coming for years whereas you started working here since last month."
		<< RESET << std::endl << std::endl;
}

void	Harl::_Error(void)
{
	std::cout << RED << "This is unacceptable! I want to speak to the manager now."
		<< RESET << std::endl << std::endl;
}