/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   NewZombie.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/23 10:54:29 by sada-sil          #+#    #+#             */
/*   Updated: 2023/11/23 13:35:02 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../zombie.h"

Zombie*	NewZombie(std::string name)
{
	Zombie *newZombie = new Zombie(name);
	return newZombie;
}