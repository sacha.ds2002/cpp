/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   RandomChump.cpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/23 10:54:31 by sada-sil          #+#    #+#             */
/*   Updated: 2023/11/23 13:35:07 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../zombie.h"

void	RandomChump(std::string name)
{
	Zombie	tmpZombie(name);
	tmpZombie.Announce();
}
