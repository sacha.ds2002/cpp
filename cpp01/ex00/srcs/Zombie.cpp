/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Zombie.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/23 10:54:43 by sada-sil          #+#    #+#             */
/*   Updated: 2023/11/23 13:35:12 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../zombie.h"

// Default constructor
Zombie::Zombie(void)
{
    std::cout << "Default constructor called" << std::endl;
    return ;
}

//Copy contructor
Zombie::Zombie(std::string name) : _name(name)
{
    std::cout << GREEN << _name << " created!" << RESET << std::endl;
    return ;
}

// Destructor
Zombie::~Zombie(void)
{
    std::cout << RED << _name << " destroyed!" << RESET << std::endl;
    return ;
}

void	Zombie::Announce(void) const
{
	std::cout << BLUE << this->_name << WHITE << ": " << "BraiiiiiiinnnzzzZ..." 
		<< RESET << std::endl;
}

