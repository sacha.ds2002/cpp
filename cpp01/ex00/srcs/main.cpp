/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/23 11:15:33 by sada-sil          #+#    #+#             */
/*   Updated: 2023/11/23 13:34:59 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../zombie.h"

int	main(void)
{
	Zombie *zombie = NewZombie("Gerald");
	(*zombie).Announce();
	std::cout << YELLOW << "----Random chump start----" << RESET << std::endl;
	RandomChump("Albert");
	std::cout << YELLOW << "----Random chump end------" << RESET << std::endl;
	delete zombie;
	return 0;
}