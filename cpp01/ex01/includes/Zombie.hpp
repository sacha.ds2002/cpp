/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Zombie.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/23 10:54:47 by sada-sil          #+#    #+#             */
/*   Updated: 2023/11/23 13:39:16 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ZOMBIE_HPP
# define ZOMBIE_HPP
# include "../zombie.h"

class Zombie
{
    public:
        Zombie(void);
		Zombie(std::string name);
        ~Zombie();
		void SetName(std::string name);
		void Announce(void) const;
	
	private:
		std::string _name;
};

#endif

