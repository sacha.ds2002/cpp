/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/23 11:15:33 by sada-sil          #+#    #+#             */
/*   Updated: 2023/11/23 13:52:53 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../zombie.h"

int	main(void)
{
	int	n = 5;
	Zombie *horde = ZombieHorde(n, "Arthur");

	for (int i = 0; i < n; i++)
		horde[i].Announce();
	delete[] horde;
	return 0;
}