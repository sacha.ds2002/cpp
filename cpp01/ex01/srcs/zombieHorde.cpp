/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   zombieHorde.cpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/23 13:33:25 by sada-sil          #+#    #+#             */
/*   Updated: 2023/11/23 14:01:07 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../zombie.h"

Zombie	*ZombieHorde(int n, std::string name)
{
	Zombie	*horde = new Zombie[n];
	
	while (--n >= 0)
		horde[n].SetName(name);
	return horde;
}