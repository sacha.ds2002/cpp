/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/24 11:29:09 by sada-sil          #+#    #+#             */
/*   Updated: 2023/11/28 09:44:55 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../replace.h"

int	check_args(int argc, char *argv[])
{
	if (argc != 4)
	{
		std::cout << RED << "You need to specify three parameters." << std::endl
			<< "In order: The filename, the string to replace, the replacement." << RESET << std::endl;
		return 1;
	}
	if (((std::string)argv[2]).compare((std::string)argv[3]) == 0)
	{
		std::cout << RED << "The string replacement is the same as the string to replace." << RESET << std::endl;
		return 1;
	}
	return 0;
}

int	main(int argc, char *argv[])
{
	std::string 		str1;
	std::string 		str2;
	std::string 		content;
	size_t 				pos;
	std::ostringstream 	buffer;

	// Check the arguments
	if (check_args(argc, argv) != 0)
		return 1;

	// Convert argv into strings for strings manipulation
	str1 = argv[2];
	str2 = argv[3];
	
	// Open file and reads its entire content then insert it inside the content string
	std::ifstream ifs(argv[1]);
	buffer << ifs.rdbuf();
	content = buffer.str();
	ifs.close();

	if (content.empty())
	{
		std::cout << RED << "The file is either empty or doesn't exist." << RESET << std::endl;
		return 1;
	}
	// While there is string to replace in the file content:
	// Replace it using erase and insert functions
	//  != std::string::npos is used to check if the content.find() function failed
	while ((pos = content.find(argv[2])) != std::string::npos)
	{
		content.erase(pos, str1.length());
		content.insert(pos, str2);
	}
	
	// Open/Create the destination file, and write the new content in it
	std::ofstream ofs(((std::string)argv[1]) + ".replace");
	ofs.write(content.c_str(), content.length());
	ofs.close();
	return 0;
}